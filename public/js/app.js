$(document).ready(function () {

    var value = $("option:selected", '#parent').val();
    var check2 = $.inArray(value, ["1", "2", "3", "4"]);
    var check = $.inArray(value, myArray);
    if (check > -1) {
        $('.sound').show();
    }
    else {
        $('.sound').hide();
    }
    if (check2 > -1) {
        $('.thumbnailCat').css({"width": "250px", "height": "400px"});
        $('.image').html('Main picture* (409x1408)');
    }
    else {
        $('.thumbnailCat').css({"width": "200px", "height": "300px"});
        $('.image').html('Main picture* (min 458x621)');
    }

    style = "width: 704px; height: 204px;"

    var Privileges = jQuery('#parent');
    var select = this.value;

    Privileges.change(function () {
        var value = $("option:selected", this).val();
        var check2 = $.inArray(value, ["1", "2", "3", "4"]);
        var check = $.inArray(value, myArray);

        if (check > -1) {
            $('.sound').show();
        }
        else {
            $('.sound').hide();
            $('#sound_reduction').val("");
            $('#sound_absorption').val("");
        }

        if (check2 > -1) {
            $('.thumbnailCat').css({"width": "250px", "height": "400px"});
            $('.image').html('Main picture* (409x1408)');
        }
        else {
            $('.thumbnailCat').css({"width": "200px", "height": "300px"});
            $('.image').html('Main picture* (min 458x621)');
        }
    });

    $('td, th', '#sortable').each(function () {
        var cell = $(this);
        cell.width(cell.width());
    });

    $("#sortable").sortable({
        axis: 'y',
        handle: '.handle',
        update: function (event, ui) {
            var data = $(this).sortable('serialize');

            var ids = [];

            $("#sortable tr").each(function () {
                ids.push($(this).data('id'));
            });

            $.post('/update-place', {ids: ids});

        }
    });

    $("#sortable").disableSelection();

    $('#add-machine, #edit-machine').submit(function (e) {

        var valid = true;
        var errorTabId = false;

        // clear old errors
        $('.error').html('');

        $("input[type='file']").each(function () {
            var $this = $(this);

            var inputName = $this.attr('name');


            var mainPicture = inputName === 'main_picture';
            var headerPicture = inputName === 'header_picture';


            var parent = $this.closest('.fileinput');
            var preview = parent.find('.fileinput-preview');
            var fileinput = parent.find('.fileinput-new');

            // main and header photos
            if (mainPicture || headerPicture) {
                if (preview.html().trim() === '' && fileinput.html().trim() === '') {
                    parent.find('.error').html('This field is required.');
                    valid = false;
                }
            }

        });
        
        if (!$('#lang_1_name').val()) {
                $('.error_lang_1_name').html('Value is required.');
                valid = false;
            }

        if ($('#catHidden').val() !== '4') {

            

            if (!$('#lang_1_material').val()) {
                $('.error_lang_1_material').html('Value is required.');
                valid = false;
            }

            if (!$('#lang_1_reaction_to_fire').val()) {
                $('.error_lang_1_reaction_to_fire').html('Value is required.');
                valid = false;
            }


            if (!$('#lang_1_thickness').val()) {

                $('.error_lang_1_thickness').html('Value is required.');
                valid = false;
            }

            if (!$('#lang_1_colour').val()) {

                $('.error_lang_1_colour').html('Value is required.');
                valid = false;
            }

            if (!$('#lang_1_light_reflexion').val()) {

                $('.error_lang_1_light_reflexion').html('Value is required.');
                valid = false;
            }

            if (!$('#lang_1_sound_reduction').val()) {

                $('.error_lang_1_sound_reduction').html('Value is required.');
                valid = false;
            }

            if (!$('#lang_1_sound_absorption').val()) {

                $('.error_lang_1_sound_absorption').html('Value is required.');
                valid = false;
            }

            if (!$('#lang_1_resistance_to_fire').val()) {

                $('.error_lang_1_resistance_to_fire').html('Value is required.');
                valid = false;
            }

        }


        if (!(valid)) {
            $('#' + errorTabId).trigger('click');
            alert('There are some errors!');
        }

        return valid && photoRequired;
    });

    $('.remove-video').on('click', function (e) {
        e.preventDefault();
        var container = $(this).parent();
        var machine = $(this).data('machine');
        var language = $(this).data('language');
        var video = $(this).data('video');

        $.ajax({
            url: "/delete-video/" + machine + "/" + language + "/" + video
        }).done(function () {
            container.hide();
        });
    });

    $('.remove-product-file').on('click', function (e) {
        e.preventDefault();
        var container = $(this).parent();
        var machine = $(this).data('machine');
        var language = $(this).data('language');
        var i = $(this).data('i');
        var type = $(this).data('type');

        $.ajax({
            url: "/delete-file/" + machine + "/" + language + "/" + i + "/" + type
        }).done(function (data) {
            container.hide();
        });
    });

    $('.remove-category-pdf').on('click', function (e) {
        e.preventDefault();
        var container = $(this).parent();
        var id = $(this).data('id');
        var type = $(this).data('type');
        console.log(id);
        $.ajax({
            url: "/delete-category-file/" + id + "/" + type
        }).done(function (data) {
            container.hide();

            window.location.reload();
        });
    });

    $('.remove-news-file').on('click', function (e) {
        e.preventDefault();
        var container = $(this).parent();
        var id = $(this).data('id');
        var i = $(this).data('i');
        var type = $(this).data('type');
        console.log(id);
        $.ajax({
            url: "/delete-news-file/" + id + "/" + i + "/" + type
        }).done(function (data) {
            container.hide();
        });
    });

    $('.show-crop-modal').on('click', function () {
        var image = $(this).closest('.fileinput').find('.original-image').clone();

        console.log(image.data - image - name);
        image.removeClass('hidden');
        image.attr('id', 'target-image');

        $('.modal-body').html('');
        $('.modal-body').append(image);

        $('#target-image').Jcrop({
            boxWidth: 460,
            boxHeight: 620,
            setSelect: [586, 815, 0, 0],
            allowResize: false,
            allowSelect: false,
            onSelect: updateCoords
        });

        $('#cropImageModal').modal('show');
    });

    var coordX, coordY, width, height;

    function updateCoords(c) {
        coordX = c.x;
        coordY = c.y;
        width = c.w;
        height = c.h;
    }

    $('.crop-image').on('click', function () {
        var url = "/create-thumbnail";
        var image = $(this).closest('#cropImageModal').find('img');

        var machineId = image.data('machine-id');
        var imageName = image.data('image-name');
        var type = image.data('type');

        $.ajax({
//            type: "POST",
            url: url,
            data: {
                'x': coordX,
                'y': coordY,
                'w': width,
                'h': height,
                'machineId': machineId,
                'imageName': imageName,
                'type': type
            },
            success: function () {
                window.location.reload();
            }
        });
    });

    $('#cropImageModal').on('show.bs.modal shown.bs.modal', function () {
        $(this).find('.modal-dialog').css({
            width: 458 + 40,
            height: 612 + 40
        });
    });

    $('.remove-image').on('click', function () {
        var type = $(this).data('type');
        if (type == 'news')
            var url = "/remove-image-news";
        else if (type == 'projects')
            var url = "/remove-image-projects";
        else
            var url = "/remove-image";
        var machineId = $(this).data('machine-id');
        var imageName = $(this).data('image-name');

        console.log(machineId, imageName);

        $.ajax({
//            type: "POST",
            url: url,
            data: {
                'machineId': machineId,
                'imageName': imageName
            },
            success: function () {
                window.location.reload();
            }
        });
    });

    $('.fileinput-preview').bind('DOMNodeInserted DOMNodeRemoved', function () {
        $(this).closest('.fileinput').find('.thumbnail-warning').css({
            visibility: 'visible'
        });
    });


});