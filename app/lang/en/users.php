<?php

return array(
    'username' => 'Username',
    'email' => 'Email',    
    'edit' => 'Edit User',
    'create' => 'Create User',
    'delete' => 'Delete User',
    'password' => 'Password',
    'password_confirmation' => 'Password Confirmation',
    'activate_user' => 'Activate User',
    'roles' => 'Roles',
    'roles_info' => 'Roles Info',
    'yes' => 'Yes',
    'no' => 'No',
    'msg' => array(
        'delete' => 'User sucessfully deleted',
        'edit' => 'User sucessfully updated',
        'create' => 'User sucessfully created',
    )
);  
