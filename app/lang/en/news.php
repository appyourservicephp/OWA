<?php

return array(
    'headline' => 'Add New Article',
    'title' => 'Title',
    'content' => 'Content',
    'picture' => 'Picture',
    'parent' => 'Choose Parent',
    'create' => 'Create Article',
    'edit' => 'Edit Article',
    'delete' => 'Delete Article',
    'news' => array(
        'create' => 'You have successfully created a new article!',
        'edit' => 'The article was updated.',
        'delete' => 'You have deleted an article!',
        
    ),
);  
