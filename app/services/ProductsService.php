<?php

class ProductsService {

    public function store($inputs) {


        $this->validateInputs(
                $inputs, Product::getPhotoRules(), Product::getVideoRules(), Product::getPdfRules(), Product::getDocxRules()
        );

        $place = Product::where('category_id', '=', $inputs['category_id'])->max('place');

        if ($place == null) {
            $place = 1;
        } else {
            $place += 1;
        }

        // create new Product
        $machine = Product::create(array('category_id' => $inputs['category_id'],
                    'place' => $place));


        $this->saveFiles(
                $machine->id, $inputs['main_picture'], $inputs['header_picture'], $inputs['pictures']
        );


        // add machine languages
        foreach ($inputs as $index => $input) {
            if (substr($index, 0, 5) == "lang_") {
                $i = 1;
                $input['machine_id'] = $machine->id;
                // save pdf file
                foreach ($inputs['pdf'] as $pdf) {
                    if (isset($pdf)) {
                        $pdf->move('machines/' . $machine->id, Language::find($input['language_id'])->slug . '_pdf_' . $i . '.pdf');
                    }
                    $i++;
                }
                $a = 1;
                foreach ($inputs['videos'] as $video) {

                    if (isset($video)) {
                        $video->move('machines/' . $machine->id, Language::find($input['language_id'])->slug . '_video_' . $a . '.mp4');
                    }
                    $a++;
                }
                if ($inputs['category_id'] !== '5') {
                    $d = 1;
                    foreach ($inputs['docx'] as $docx) {

                        if (isset($docx)) {
                            $docx->move('machines/' . $machine->id, Language::find($input['language_id'])->slug . '_doc_' . $d . '.' . $docx->getClientOriginalExtension());
                        }
                        $d++;
                    }
                }

               
                MachineLanguages::create($input);
            }
        }

        return $machine;
    }

    private function validateInputs($inputs, $photoRules, $videoRules, $pdfRules, $docxRules, $pdfsToSkipValidation = array()) {
        $validators = array();


        // validate photos
        Validator::extend('requireOnePhoto', 'PhotoValidator@requireOnePhoto');
        Validator::extend('resolution', 'PhotoValidator@resolution');
        Validator::extend('type', 'PhotoValidator@type');
        $messages = array(
            'require_one_photo' => 'Please upload at least one photo with right resolution.',
            'resolution' => 'Resolution is not right.',
            'type' => 'Please upload a image file.'
        );

        Validator::extend('video', 'VideoValidator@video');
        $videoValidatorMessages = array(
            'video' => 'Please upload mp4 video file.',
        );


        $photoValidator = Validator::make(
                        $inputs, $photoRules, $messages
        );

        if ($photoValidator->fails()) {
            $validators['photo'] = $photoValidator;           
        }

        Validator::extend('pdf', 'PdfValidator@pdf');
        $messages = array(
            'pdf' => 'Please upload pdf file.',
        );

        Validator::extend('docx', 'DocxValidator@docx');
        $messages = array(
            'docx' => 'Please upload a word document (.docx).',
        );


        $requiredLanguages = Language::getRequiredLanguagesIds();

        foreach ($inputs as $index => $input) {

            // language inputs have multiple fields
            if (substr($index, 0, 5) == "lang_") {
                
                // validate pdf
                for ($i = 1; $i < 4; $i++) {

                    if (isset($inputs['pdf'][$i]) && isset($inputs['pdf'][$i]) != '') {

                        $pdfInput = array('pdf' => $inputs['pdf'][$i]);

                        $pdfValidator = Validator::make(
                                        $pdfInput, $pdfRules, $messages
                        );

                        if ($pdfValidator->fails()) {
                            $validators['pdf' . $i] = $pdfValidator;
                        }
                    }
                }

                // validate docx
                for ($i = 1; $i < 4; $i++) {

                    if (isset($inputs['docx'][$i]) && isset($inputs['docx'][$i]) != '') {

                        $docxInput = array('docx' => $inputs['docx'][$i]);

                        $docxValidator = Validator::make(
                                        $docxInput, $docxRules, $messages
                        );

                        if ($docxValidator->fails()) {
                            $validators['docx' . $i] = $docxValidator;
                        }
                    }
                }

                // validate videos
                for ($i = 1; $i < 4; $i++) {
                    if (isset($inputs['videos'][$i]) && isset($inputs['videos'][$i]) != '') {

                        $videoInput = array('video' => $inputs['videos'][$i]);
                        $videoValidator = Validator::make(
                                        $videoInput, $videoRules, $videoValidatorMessages
                        );

                        if ($videoValidator->fails()) {
                            $validators['video' . $i] = $videoValidator;
                        }
                    }
                }
                if ($inputs['category_id'] !== '5') {
                    $inputText = array(
                        'name' => $input['name'],
                        'material' => $input['material'],
                        'reaction_to_fire' => $input['reaction_to_fire'],
                        'thickness' => $input['thickness'],
                        'colour' => $input['colour'],
                        'light_reflexion' => $input['light_reflexion'],
                        'sound_reduction' => $input['sound_reduction'],
                        'sound_absorption' => $input['sound_absorption'],
                        'resistance_to_fire' => $input['resistance_to_fire'],
                        
                    );

                    $rules = array(
                        'name' => 'required',
                        'material' => 'required',
                        'reaction_to_fire' => 'required',
                        'thickness' => 'required',
                        'colour' => 'required',
                        'light_reflexion' => 'required',
                        'sound_reduction' => 'required',
                        'sound_absorption' => 'required',
                        'resistance_to_fire' => 'required',
                    );
                } else {
                   
                    $inputText = array(
                        'name' => $input['name'],
                        'intro' => $input['intro'],
                    );
                }

                $rules = array(
                     'name' => 'required',
                   
                );




                $inputTextValidator = Validator::make($inputText, $rules);

                if ($inputTextValidator->fails()) {
                    $validators['lang_' . $input['language_id'] . '[name]' . $i] = $inputTextValidator;
                    $validators['lang_' . $input['language_id'] . '[material]' . $i] = $inputTextValidator;
                    $validators['lang_' . $input['language_id'] . '[reaction_to_fire]' . $i] = $inputTextValidator;
                    $validators['lang_' . $input['language_id'] . '[thickness]' . $i] = $inputTextValidator;
                    $validators['lang_' . $input['language_id'] . '[colour]' . $i] = $inputTextValidator;
                    $validators['lang_' . $input['language_id'] . '[light_reflexion]' . $i] = $inputTextValidator;
                    $validators['lang_' . $input['language_id'] . '[sound_reduction]' . $i] = $inputTextValidator;
                    $validators['lang_' . $input['language_id'] . '[sound_absorption]' . $i] = $inputTextValidator;
                    $validators['lang_' . $input['language_id'] . '[resistance_to_fire]' . $i] = $inputTextValidator;
                   
                }

                //validate inputs
            }
        }

        if (count($validators) > 0) {
          
            throw new ValidatorException($validators);
        }
    }

    public function update($inputs) {
        $photoRules = Product::getPhotoRules();
        $videoRules = Product::getVideoRules();
        $pdfRules = Product::getPdfRules();
        $docxRules = Product::getDocxRules();

        $machineUpdate = false;
        $languageUpdate = array();

        // if main picture is not provided skip validation
        if (!isset($inputs['main_picture'])) {
            unset($photoRules['main_picture']);
        } else {
            $machineUpdate = true;
        }

        // if header picture is not provided skip validation
        if (!isset($inputs['header_picture'])) {
            unset($photoRules['header_picture']);
        } else {
            $machineUpdate = true;
        }

        // if pictures is not provided skip validation
        if (isset($inputs['pictures'])) {
            $countPictures = 0;
            foreach ($inputs['pictures'] as $value) {
                if ($value === null) {
                    $countPictures += 1;
                }
            }

            if ($countPictures == 10) {
                unset($photoRules['pictures']);
            } else {
                $machineUpdate = true;
            }

            foreach ($inputs['pictures'] as $index => $value) {
                if (isset($inputs['machine_id']) && $value === '' && $countPictures > 1) {
                    unset($photoRules['pictures']);
                    $dirPath = public_path() . '/machines/' . $inputs['machine_id'];
                    $picturePath = $dirPath . '/photo_' . $index . '.jpg';
                    if (file_exists($picturePath)) {
                        unlink($picturePath);
                    }
                    $pictureThumbnailPath = $dirPath . '/photo_' . $index . '_thumbnail.jpg';
                    if (file_exists($pictureThumbnailPath)) {
                        unlink($pictureThumbnailPath);
                    }
                    $machineUpdate = true;
                }
            }
        } else {
            unset($photoRules['pictures']);
        }

        // if pdf are not provided skip validation
        if (isset($inputs['pdf'])) {
            $countPdf = 0;
            foreach ($inputs['pdf'] as $value) {
                if ($value === null) {
                    $countPdf += 1;
                }
            }

            if ($countPdf == 3) {
                unset($pdfRules['pdf']);
            } else {
                $machineUpdate = true;
            }

            foreach ($inputs['pdf'] as $index => $value) {
                if (isset($inputs['machine_id']) && $value === '' && $countPdf > 1) {
                    unset($pdfRules['pdf']);
                    $dirPath = public_path() . '/machines/' . $inputs['machine_id'];
                    $pdfPath = $dirPath . '/pdf_' . $index . '.pdf';
                    if (file_exists($pdfPath)) {
                        unlink($pdfPath);
                    }

                    $machineUpdate = true;
                }
            }
        } else {
            unset($pdfRules['pdf']);
        }




        $pdfsToSkipValidation = array();
        foreach ($inputs as $index => $input) {
            if (substr($index, 0, 5) == "lang_") {

                // pdf
                for ($i = 1; $i < 4; $i++) {
                    if (isset($input['pdf'][$i])) {
                        foreach ($input['pdf'] as $pdf) {
                            if ($pdf == null && isset($inputs['machine_id'])) {

                                $originalName = Language::find($input['language_id'])->slug;
                                $path = public_path() . '/machines/' . $inputs['machine_id'] . '/' . $originalName . '_pdf_' . $i . '.pdf';

                                if (file_exists($path)) {
                                    $pdfsToSkipValidation[] = $input['language_id'];
                                }
                            } else {
                                $languageUpdate[] = $input['language_id'];
                            }
                        }
                    }
                }
            }
        }



        // validate inputs
        $this->validateInputs($inputs, $photoRules, $videoRules, $pdfRules, $docxRules, $pdfsToSkipValidation);


        $machineId = $inputs['machine_id'];

        $machineLanguages = MachineLanguages::where('machine_id', '=', $machineId)->get();

        $this->saveFiles(
                $machineId, $inputs['main_picture'], $inputs['header_picture'], $inputs['pictures']
        );



        // update specs values
        foreach ($machineLanguages as $machineLanguage) {

            $index = 'lang_' . $machineLanguage->language_id;

            $specs = $inputs[$index];
            unset($specs['pdf']);
            unset($specs['videos']);

            $oldAttributes = $machineLanguage->getAttributes();
            unset($oldAttributes['id']);
            unset($oldAttributes['machine_id']);
            unset($oldAttributes['created_at']);
            unset($oldAttributes['updated_at']);


            $machineLanguage->update($specs);



            // edit pdf
            for ($i = 1; $i < 4; $i++) {
                $pdf = $inputs['pdf'][$i];
                if ($pdf !== null) {
                    $dirPath = public_path() . '/machines/' . $machineId;
                    $pdfPath = $dirPath . '/' . Language::find($machineLanguage->language_id)->slug . '_pdf_' . $i . '.pdf';
                    if (file_exists($pdfPath)) {
                        unlink($pdfPath);
                    }
                    $pdf->move($dirPath, '/' . Language::find($machineLanguage->language_id)->slug . '_pdf_' . $i . '.pdf');
                }
            }
            if ($inputs['category_id'] !== '5') {
                // edit docx
                for ($i = 1; $i < 4; $i++) {
                    $docx = $inputs['docx'][$i];
                    if ($docx !== null) {
                        $dirPath = public_path() . '/machines/' . $machineId;
                        $docxPath = $dirPath . '/' . Language::find($machineLanguage->language_id)->slug . '_doc_' . $i . '.' . $docx->getClientOriginalExtension();
                        if (file_exists($docxPath)) {
                            unlink($docxPath);
                        }
                        $docx->move($dirPath, '/' . Language::find($machineLanguage->language_id)->slug . '_doc_' . $i . '.' . $docx->getClientOriginalExtension());
                    }
                }
            }
            // edit video
            for ($i = 1; $i < 4; $i++) {
                $video = $inputs['videos'][$i];
                if ($video !== null) {
                    $dirPath = public_path() . '/machines/' . $machineId;
                    $videoPath = $dirPath . '/' . Language::find($machineLanguage->language_id)->slug . '_video_' . $i . '.mp4';
                    if (file_exists($videoPath)) {
                        unlink($videoPath);
                    }
                    $video->move($dirPath, '/' . Language::find($machineLanguage->language_id)->slug . '_video_' . $i . '.mp4');
                }
            }




            foreach ($oldAttributes as $key => $attr) {

                if (isset($key) && isset($specs[$key])) {

                    if ($attr !== $specs[$key]) {
                        $languageUpdate[] = $machineLanguage->language_id;
                    }
                }
            }
        }

        return array(
            'machineUpdate' => $machineUpdate,
            'languageUpdate' => $languageUpdate
        );
    }

    public function saveFiles($machineId, $mainPicture, $headerPicture, $pictures) {
        // save main image
        if (isset($mainPicture)) {

            $mainPicture->move('machines/' . $machineId, 'main_picture.jpg');
            $img = Image::make('machines/' . $machineId . '/main_picture.jpg');

            if ($img->height() >= $img->width()) {

                $img->fit(815, 1222);
            } else {
                $img->fit(1222, 815);
            }
            $img->save('machines/' . $machineId . '/main_picture.jpg');

            $thumbnailImg = Image::make('machines/' . $machineId . '/main_picture.jpg');
            $thumbnailImg->fit(460, 620);
            $thumbnailImg->save(public_path() . '/machines/' . $machineId . '/main_picture_thumbnail.jpg');
        }

        // save header image
        if (isset($headerPicture)) {
            $headerPicture->move('machines/' . $machineId, 'header_picture.jpg');
        }


        // save photos
        $i = 1;
        foreach ($pictures as $index => $picture) {
            if ($picture != null) {
                $picture->move('machines/' . $machineId, 'photo_' . $i . '.jpg');

                $img = Image::make('machines/' . $machineId . '/photo_' . $i . '.jpg');
                if ($img->height() >= $img->width()) {

                    $img->fit(815, 1222);
                } else {

                    $img->fit(1222, 815);
                }
                $img->save('machines/' . $machineId . '/photo_' . $i . '.jpg');

                $thumbnailImg = Image::make('machines/' . $machineId . '/photo_' . $i . '.jpg');
                $thumbnailImg->fit(627, 940);
                $thumbnailImg->save(public_path() . '/machines/' . $machineId . '/photo_' . $i . '_thumbnail.jpg');
            }
            $i += 1;
        }
    }

    public function deleteFiles($machine_id) {
        $dirPath = public_path() . '/machines/' . $machine_id;
        $language = 'english';
        // delete main picture
        $mainPicturePath = $dirPath . '/main_picture.jpg';
        if (file_exists($mainPicturePath)) {
            unlink($mainPicturePath);
        }

        // delete header picture
        $headerPicturePath = $dirPath . '/header_picture.jpg';
        if (file_exists($headerPicturePath)) {
            unlink($headerPicturePath);
        }

        $mainPictureThumbnailPath = $dirPath . '/main_picture_thumbnail.jpg';
        if (file_exists($mainPictureThumbnailPath)) {
            unlink($mainPictureThumbnailPath);
        }


        // delete photos
        for ($i = 1; $i < 11; $i++) {
            $picturePath = $dirPath . '/photo_' . $i . '.jpg';
            if (file_exists($picturePath)) {
                unlink($picturePath);
            }
            $pictureThumbnailPath = $dirPath . '/photo_' . $i . '_thumbnail.jpg';
            if (file_exists($pictureThumbnailPath)) {
                unlink($pictureThumbnailPath);
            }
        }

        for ($i = 1; $i < 4; $i++) {

            // delete docx
            $docxPath = $dirPath . '/' . $language . '_doc_' . $i . '.docx';
            if (file_exists($docxPath)) {
                unlink($docxPath);
            }

            // delete video
            $videoPath = $dirPath . '/' . $language . '_video_' . $i . '.mp4';
            if (file_exists($videoPath)) {
                unlink($videoPath);
            }

            // delete pdf
            $pdfPath = $dirPath . '/' . $language . '_pdf_' . $i . '.pdf';
            if (file_exists($pdfPath)) {
                unlink($pdfPath);
            }
        }

        // delete directory
        if (is_dir($dirPath)) {
            rmdir($dirPath);
        }
    }

    public function deleteVideo($machine, $language, $video) {
        $dirPath = public_path() . '/machines/' . $machine;

        $videoPath = $dirPath . '/' . $language . '_video_' . $video . '.mp4';
        if (file_exists($videoPath)) {
            unlink($videoPath);
        }

        return file_exists($videoPath) ? false : true;
    }

    public function deleteFile($machine, $language, $i, $type) {
        $dirPath = public_path() . '/machines/' . $machine;
        if ($type == 'pdf') {
            $pdfPath = $dirPath . '/' . $language . '_pdf_' . $i . '.pdf';
            if (file_exists($pdfPath)) {
                unlink($pdfPath);
            }
            return file_exists($pdfPath) ? false : true;
        } else if ($type == 'video') {
            $videoPath = $dirPath . '/' . $language . '_video_' . $i . '.mp4';
            if (file_exists($videoPath)) {
                unlink($videoPath);
            }
            return file_exists($videoPath) ? false : true;
        } else {
            $docxPath = $dirPath . '/' . $language . '_doc_' . $i . '.docx';
            if (file_exists($docxPath)) {
                unlink($docxPath);
            }
            return file_exists($docxPath) ? false : true;
        }
    }

    protected function formatOptions($machineOptions, $machineOptionLabel) {
        
    }

}
