<?php

class ArticlesService {

    public function store($inputs, $entityName) {

        $this->validateInputs(
                $inputs, Article::getPhotoRules(), Article::getVideoRules(), Article::getPdfRules()
        );
        // create new Product
        $product = Product::create(array('category_id' => $inputs['id']));
        $article = new Article();
        $article->user_id = Auth::user()->id;
        $article->title = $inputs['title'];
        $article->content = $inputs['content'];
        $article->category_id = $inputs['id'];
        $article->machine_id = $product->id;
        $article->save();

        
        
        
        $this->saveFiles(
                $article->id, $inputs['pictures'], $inputs['header_picture']
        );


//        for ($i = 1; $i < 3; $i++) {
//            $image = $inputs['image'][$i];
//            if ($image !== null) {
//                $image->move('appfiles/news/images/' . $article->id, $article->id . '_images_' . $i . '.jpg');
//            }
//        }

        for ($i = 1; $i < 3; $i++) {
            $video = $inputs['video'][$i];
            if ($video !== null) {
                $video->move('appfiles/news/' . $article->id, $article->id . '_video_' . $i . '.mp4');
            }
        }

        for ($i = 1; $i < 3; $i++) {
            $pdf = $inputs['pdf'][$i];
            if ($pdf !== null) {
                $pdf->move('appfiles/news/' . $article->id, $article->id . '_pdf_' . $i . '.pdf');
            }
        }



        return $article;
    }

    private function validateInputs($inputs, $photoRules, $videoRules, $pdfRules) {

        $validators = array();

        // validate photos
        Validator::extend('requireOnePhoto', 'PhotoValidator@requireOnePhoto');
        Validator::extend('resolution', 'PhotoValidator@resolution');
        Validator::extend('type', 'PhotoValidator@type');
        $messages = array(
            'require_one_photo' => 'Please upload at least one photo with right resolution.',
            'resolution' => 'Resolution is not right.',
            'type' => 'Please upload a image file.'
        );

        Validator::extend('video', 'VideoValidator@video');
        $videoValidatorMessages = array(
            'video' => 'Please upload mp4 video file.',
        );


        $photoValidator = Validator::make(
                        $inputs, $photoRules, $messages
        );

        if ($photoValidator->fails()) {
            $validators['photo'] = $photoValidator;
        }

        Validator::extend('pdf', 'PdfValidator@pdf');
        $messages = array(
            'pdf' => 'Please upload pdf file.',
        );

        foreach ($inputs as $index => $input) {

            // validate pdf
            for ($i = 1; $i < 3; $i++) {

                if (isset($inputs['pdf'][$i]) && isset($inputs['pdf'][$i]) != '') {

                    $pdfInput = array('pdf' => $inputs['pdf'][$i]);

                    $pdfValidator = Validator::make(
                                    $pdfInput, $pdfRules, $messages
                    );

                    if ($pdfValidator->fails()) {
                        $validators['pdf' . $i] = $pdfValidator;
                    }
                }
            }

            for ($i = 1; $i < 3; $i++) {

                // validate video
                if (isset($inputs['video'][$i]) && isset($inputs['video'][$i]) != '') {

                    $videoInput = array('video' => $inputs['video'][$i]);

                    $videoValidator = Validator::make(
                                    $videoInput, $videoRules, $videoValidatorMessages
                    );
                    if ($videoValidator->fails()) {

                        $validators['video' . $i] = $videoValidator;
                    }
                }
            }


            // validate content

            $inputText = array(
                'title' => $inputs['title'],
                'content' => $inputs['content'],
            );



            $rules = array(
                'title' => 'required',
                'content' => 'required',
            );



            $inputTextValidator = Validator::make($inputText, $rules);

            if ($inputTextValidator->fails()) {

                $validators['title'] = $inputTextValidator;
                $validators['content'] = $inputTextValidator;
            }


            //validate inputs
        }


        if (count($validators) > 0) {

            throw new ValidatorException($validators);
        }
    }

    public function update($inputs, $id) {
        $photoRules = Article::getPhotoRules();
        $videoRules = Article::getVideoRules();
        $pdfRules = Article::getPdfRules();

        $machineUpdate = false;

        // if header picture is not provided skip validation
        if (!isset($inputs['header_picture'])) {
            unset($photoRules['header_picture']);
        } else {
            $machineUpdate = true;
        }

        // if pictures is not provided skip validation
        if (isset($inputs['pictures'])) {
            $countPictures = 0;
            foreach ($inputs['pictures'] as $value) {
                if ($value === null) {
                    $countPictures += 1;
                }
            }

            if ($countPictures == 5) {
                unset($photoRules['pictures']);
            } else {
                $machineUpdate = true;
            }

            foreach ($inputs['pictures'] as $index => $value) {
                if (isset($inputs['machine_id']) && $value === '' && $countPictures > 1) {
                    unset($photoRules['pictures']);
                    $dirPath = public_path() . '/machines/' . $inputs['machine_id'];
                    $picturePath = $dirPath . '/photo_' . $index . '.jpg';
                    if (file_exists($picturePath)) {
                        unlink($picturePath);
                    }
                    $pictureThumbnailPath = $dirPath . '/photo_' . $index . '_thumbnail.jpg';
                    if (file_exists($pictureThumbnailPath)) {
                        unlink($pictureThumbnailPath);
                    }
                    $machineUpdate = true;
                }
            }
        } else {
            unset($photoRules['pictures']);
        }

        $this->validateInputs($inputs, $photoRules, $videoRules, $pdfRules);

        $this->saveFiles(
                $id, $inputs['pictures'], $inputs['header_picture']
        );

        // edit pdf
        for ($i = 1; $i < 3; $i++) {
            $pdf = $inputs['pdf'][$i];
            if ($pdf !== null) {
                $dirPath = public_path() . '/appfiles/news/' . $id;
                $pdfPath = $dirPath . '/' . $id . '_pdf_' . $i . '.pdf';
                if (file_exists($pdfPath)) {
                    unlink($pdfPath);
                }
                $pdf->move('appfiles/news/' . $id, $id . '_pdf_' . $i . '.pdf');
            }
        }


        // edit video
        for ($i = 1; $i < 3; $i++) {
            $video = $inputs['video'][$i];
            if ($video !== null) {
                $dirPath = public_path() . '/appfiles/news/' . $id;

                $videoPath = $dirPath . '/' . $id . '_video_' . $i . '.mp4';

                if (file_exists($videoPath)) {

                    unlink($videoPath);
                }
                $video->move('appfiles/news/' . $id, $id . '_video_' . $i . '.mp4');
            }
        }



//        $pdfsToSkipValidation = array();        
//            // pdf
//            for ($i = 1; $i < 3; $i++) {
//                if (isset($inputs['pdf'][$i])) {
//                    foreach ($inputs['pdf'] as $pdf) {
//                        if ($pdf == null && isset($id)) {
//
//                            $path = public_path() . '/appfiles/pdf/' . $id . '/'. $id . '_pdf_' . $i . '.pdf';
//
//                            if (file_exists($path)) {
//                                $pdfsToSkipValidation[] = $id;
//                            }
//                        }
//                    }
//                }
//            }
        // validate inputs


        $article = Article::find($id);
        $article->user_id = Auth::user()->id;
        $article->title = $inputs['title'];
        $article->content = $inputs['content'];
        $article->save();
    }

    public function saveFiles($id, $pictures, $headerPicture) {

        // save photos
//        $i = 1;
//        foreach ($pictures as $index => $picture) {
//            if ($picture != null) {
//                $picture->move('appfiles/news/images/' . $id . '_photo_' . $i . '.jpg');
//            }
//            $i += 1;
//        }

        if (isset($headerPicture)) {
            $headerPicture->move('appfiles/news/' . $id, 'header_picture.jpg');
        }

        $i = 1;
        foreach ($pictures as $index => $picture) {
            if ($picture != null) {
                $picture->move('appfiles/news/' . $id, '/' . $id . '_photo_' . $i . '.jpg');

                $img = Image::make('appfiles/news/' . $id . '/' . $id . '_photo_' . $i . '.jpg');
                if ($img->height() >= $img->width()) {

                    $img->fit(815, 1222);
                } else {

                    $img->fit(1222, 815);
                }
                $img->save('appfiles/news/' . $id . '/' . $id . '_photo_' . $i . '.jpg');

                $thumbnailImg = Image::make('appfiles/news/' . $id . '/' . $id . '_photo_' . $i . '.jpg');
                $thumbnailImg->fit(627, 940);
                $thumbnailImg->save(public_path() . '/appfiles/news/' . $id . '/' . $id . '_photo_' . $i . '_thumbnail.jpg');
            }
            $i += 1;
        }
    }

    public function deleteFiles($id) {
        $dirPath = public_path() . '/appfiles/news/' . $id;

        $headerPicturePath = $dirPath . '/header_picture.jpg';
        if (file_exists($headerPicturePath)) {
            unlink($headerPicturePath);
        }

        for ($i = 1; $i < 6; $i++) {
            // delete photos
            $picturePath = $dirPath . '/' . $id . '_photo_' . $i . '.jpg';
            if (file_exists($picturePath)) {
                unlink($picturePath);
            }

            // delete thumbnail
            $thumbnailPath = $dirPath . '/' . $id . '_photo_' . $i . '_thumbnail' . '.jpg';
            if (file_exists($thumbnailPath)) {
                unlink($thumbnailPath);
            }

            // delete pdf
            $pdfPath = $dirPath . '/' . $id . '_pdf_' . $i . '.pdf';
            if (file_exists($pdfPath)) {
                unlink($pdfPath);
            }
            // delete video
            $videoPath = $dirPath . '/' . $id . '_video_' . $i . '.mp4';
            if (file_exists($videoPath)) {
                unlink($videoPath);
            }
        }

        // delete directory
        if (is_dir($dirPath)) {
			//rmdir($dirPath);
        	$this->rmdir_recursive($dirPath);
		}
    }


	private function rmdir_recursive($dir) {
		foreach(scandir($dir) as $file) {
    		if ('.' === $file || '..' === $file) continue;
    		if (is_dir("$dir/$file")) rmdir_recursive("$dir/$file");
    		else unlink("$dir/$file");
		}
		rmdir($dir);
	}

    public function deleteFile($id, $i, $type) {

        $dirPath = public_path() . '/appfiles/news/';
        if ($type == 'pdf') {

            $pdfPath = $dirPath . $id . '/' . $id . '_pdf_' . $i . '.pdf';
            if (file_exists($pdfPath)) {
                unlink($pdfPath);
            }
            return file_exists($pdfPath) ? false : true;
        } else if ($type == 'video') {
            $videoPath = $dirPath . $id . '/' . $id . '_video_' . $i . '.mp4';
            if (file_exists($videoPath)) {
                unlink($videoPath);
            }
            return file_exists($videoPath) ? false : true;
        }
    }

}
