<?php

class Logo extends Eloquent {

    protected $table = 'logo';
    protected $fillable = [ 'title', 'shortcode', 'picture'];
//    protected $file;
    protected $appends = array('file');

    public function getFileAttribute() {

        $doc = Document::where(function($query) {
                    $query->where('entity_id', $this->id)
                            ->where('entity_name', get_class());
                })->first();

        if (!empty($doc)) {

            $logoPath = public_path() . '/appfiles/logo/' . $this->id . '/logo_' . $this->id . '.' . $doc->ext;
            if (file_exists($logoPath)) {
                $res = asset('/appfiles/logo/' . $this->id . '/logo_' . $this->id . '.' . $doc->ext);
            }
            if (!empty($res)) {
                return $this->attributes['file'] = $res;
            }
        }
    }

}
