<?php

class Document extends \Eloquent {

    protected $fillable = [];

    public function store($file, $entityName, $id) {

        if (is_array($file)) {
            foreach ($file as $f) {
                $extension = $f->getClientOriginalExtension();
                $this->entity_id = $id;
                $this->entity_name = $entityName;
                $this->name = $f->getClientOriginalName();
                $this->ext = $extension;
                $this->save();
                if ($this->ext == 'pdf') {
                    $destinationPath = public_path() . '/appfiles/categories/' . $id . '/';
                    $f->move($destinationPath, $this->id . 'pdf_' . $this->entity_id . '.' . $this->ext);
                } else {
                    $destinationPath = public_path() . '/appfiles/categories/' . $id . '/';
                    $f->move($destinationPath, $this->id . 'photo_' . $this->entity_id . '.' . $this->ext);
                }
            }
        } else {
            $extension = $file->getClientOriginalExtension();
            $this->entity_id = $id;
            $this->entity_name = $entityName;
            $this->name = $file->getClientOriginalName();
            $this->ext = $extension;
            $this->save();
            if ($this->entity_name == 'Logo') {
                $destinationPath = public_path() . '/appfiles/logo/' . $id . '/';
                $file->move($destinationPath, 'logo_' . $this->entity_id . '.' . $this->ext);
            } else if ($this->ext == 'pdf') {
                $destinationPath = public_path() . '/appfiles/categories/' . $id . '/';
                $file->move($destinationPath, 'pdf_' . $this->entity_id . '.' . $this->ext);
            } else {
                $destinationPath = public_path() . '/appfiles/categories/' . $id . '/';
                $file->move($destinationPath, 'photo_' . $this->entity_id . '.' . $this->ext);
            }
        }
    }

    public function storeHeaderImage($file, $entityName, $id) {
        $extension = $file->getClientOriginalExtension();
        $this->entity_id = $id;
        $this->entity_name = $entityName;
        $this->name = $file->getClientOriginalName();
        $this->ext = $extension;
        $this->save();
        $destinationPath = public_path() . '/appfiles/categories/' . $id . '/';
        $file->move($destinationPath, 'header_picture.jpg');
    }

    public function categories() {
        return $this->belongsTo('Category', 'entity_id');
    }

}
