<?php

class Product extends Eloquent {

    protected static $photoRules = array(
        'main_picture' => 'required|image|resolution:458, 621',
        'header_picture' => 'required|image|resolution:2048, 544',
        'pictures' => 'type|requireOnePhoto:458, 621'
    );

    protected static $videoRules = array(
        'video' => 'video|max:10000',
    );
    
     protected static $pdfRules = array(
        'pdf' => 'pdf'
    );
     
     protected static $docxRules = array(
        'docx' => 'docx'
    );
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * Mass fillable fields
     *
     * @var array
     */
    protected $fillable = array(
        'category_id', 'place'
    );

    protected $hidden = array('created_at',
                              'updated_at',
                              'place');

    public function machineLanguages()
    {
        return $this->hasMany('MachineLanguages' , 'machine_id');
    }

    public function category()
    {
        return $this->belongsTo('Category');
    }

    public static function getPhotoRules()
    {
        return self::$photoRules;
    }

    public static function getVideoRules()
    {
        return self::$videoRules;
    }
    
     public static function getPdfRules()
    {
        return self::$pdfRules;
    }
    
     public static function getDocxRules()
    {
        return self::$docxRules;
    }

    
}