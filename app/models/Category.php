<?php

class Category extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';
    public static $rules = array(
        'title' => 'required|min:3',
        'content' => 'required',
        'image' => 'image|resolution:621, 458|mimes:jpeg,png',
        'pictures' => 'requireOnePhoto:458, 621',
        
        'pdf' => 'pdf',
//        'sound_reduction' => 'required',
//        'sound_absorption' => 'required',
    );
    protected $appends = array('image',
        'pdf',
        'main_picture'
       
    );

    public function getImageAttribute() {
       
        $photoPathsJpg = public_path() . '/appfiles/categories/' . $this->id . '/photo_' . $this->id . '.jpg';
        $photoPathsPng = public_path() . '/appfiles/categories/' . $this->id . '/photo_' . $this->id . '.png';
        $photoPathsJpeg = public_path() . '/appfiles/categories/' . $this->id . '/photo_' . $this->id . '.jpeg';
        if (file_exists($photoPathsJpg)) {
           $photoPathsJpg = $this->getAssetsPath( $this->id . '/photo_' . $this->id . '.jpg');
           return $this->attributes['image'] = $photoPathsJpg;
        } else if(file_exists($photoPathsPng)) {
             $photoPathsPng = $this->getAssetsPath( $this->id . '/photo_' . $this->id . '.png');
           return $this->attributes['image'] = $photoPathsPng;
        } else if(file_exists($photoPathsJpeg)) {
             $photoPathsJpeg = $this->getAssetsPath( $this->id . '/photo_' . $this->id . '.jpeg');
           return $this->attributes['image'] = $photoPathsJpeg;
        }
    }
    
    public function getMainPictureAttribute() {
        $photoPaths = public_path() . '/appfiles/categories/' . $this->id . '/main_picture_thumbnail.jpg';
        if (file_exists($photoPaths)) {
           $photoPath = $this->getAssetsPath( $this->id . '/main_picture_thumbnail.jpg');
           return $this->attributes['main_picture'] = $photoPath;
        } 
    }

    public function getPdfAttribute() {
        $pdfPaths = public_path() . '/appfiles/categories/' . $this->id . '/pdf_' . $this->id . '.pdf';
        if (file_exists($pdfPaths)) {
           $pdfPath = $this->getAssetsPath( $this->id . '/pdf_' . $this->id . '.pdf');
           return $this->attributes['pdf'] = $pdfPath;
        } 
         
    }

    public function getProductsAttribute() {
        $products = Product::where('category_id', $this->id)->get();
        foreach ($products as $product) {
            $machines = MachineLanguages::where('machine_id', $product->id)->get();
            foreach ($machines as $machine) {
                return $machine;
            }
        }
    }

    

    private function getAssetsPath($file) {
        return asset('/appfiles/categories/' . $file);
    }

    protected $hidden = array('created_at', 'updated_at');
    protected $guarded = array();

    public function subCategories() {
        return $this->hasMany('Category', 'parent_id');
    }

    public function masterCategory() {
        return $this->belongsTo('Category', 'parent_id');
    }

    public function machines() {
        return $this->hasMany('Product');
    }

    public function images() {
        return $this->hasMany('Document', 'entity_id');
    }

    public function articles() {
        return $this->hasMany('Article', 'category_id');
    }

}
