@extends('layouts/main')
<!--Web site Title--> 
@section('title') {{$title}} :: @parent @stop
@section('content')

{{$breadcrumbs}}
@include('notifications')

<h1>{{$title}}</h1>
<hr>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3>Structure</h3>
    </div>
    <div class="panel-body">
        @if(!empty($parents1))
            @foreach($parents1 as $parent)
            @if(count($parent['children'])>0)
            <div class="panel panel-default"> 
                <div class="panel-heading">{{$parent['title']}}</div>
                <div class="panel-body">
                    <ul class="list-group">
                        @foreach($parent['children'] as $children)
                        <?php $num = Product::where('category_id', $children['id'])->get(); ?>
                        <li class="list-group-item"><a href="{{'/products/'. $children['id']. '/show'}}">{{$children['title']}} <span class="badge pull-right">{{ count($num)}}</span></a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @else
            <div class="panel panel-default">
                <div class="panel-heading">{{$parent['title']}}</div>
                <div class="panel-body">                    
                    
                        <small>There are no products</small>
                   
                </div>              
            </div>
            @endif
            @endforeach
        @else
            <i>There are no products or subcategories for this category.</i>
        @endif
    </div>    
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3>Systems</h3>
    </div>
    <div class="panel-body">
        @if(!empty($parents2))
            @foreach($parents2 as $parent)
            @if(count($parent['children'])>0)
            <div class="panel panel-default"> 
                <div class="panel-heading">{{$parent['title']}}</div>
                <div class="panel-body">
                   <ul class="list-group">
                        @foreach($parent['children'] as $children)
                        <?php $num = Product::where('category_id', $children['id'])->get(); ?>
                        <li class="list-group-item"><a href="{{'/products/'. $children['id']. '/show'}}">{{$children['title']}} <span class="badge pull-right">{{ count($num)}}</span></a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @else
            <div class="panel panel-default">
                <div class="panel-heading">{{$parent['title']}}</div>
                <div class="panel-body">                    
                    
                        <small>There are no products</small>
                   
                </div>              
            </div>
            @endif
            @endforeach
        @else
            <i>There are no products or subcategories for this category.</i>
        @endif
    </div>    
</div>

@stop