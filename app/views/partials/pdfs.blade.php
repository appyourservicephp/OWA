@for ($i=1; $i < 4; $i++)
<?php
    if (count($errors)) {
        $messages = $errors->getMessageBag()->getMessages();         
        isset($messages['pdf'.$i]) ? $pdfErrors = $messages['pdf'.$i] : $pdfErrors = array();     
    }
    ?>
<div class="input-field-group pdf-input">
    <div class="col-lg-3">
        <label class="control-label pull-right" for="lang_{{$languageId}}[note]">Pdf {{$i}} @if (in_array($languageId, $requiredLanguageIds)) @endif </label>
    </div>
    <div class="col-lg-6 fileinput fileinput-new" data-provides="fileinput">
        <div class="input-group">
            <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
            <span style="background: white" class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select pdf</span><span class="fileinput-exists">Change</span><input type="file" name="pdf[{{$i}}]"></span>
            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
   
    @if ( isset($machine_id) && file_exists(public_path().'/machines/'.$machine_id.'/'.$language.'_pdf_'.$i.'.pdf'))
        <div class="pdf-info col-lg-3">
            Current pdf {{$i}}: <a href="{{asset('/machines/'.$machine_id.'/'.$language.'_pdf_'.$i.'.pdf')}}">download</a>  | <a class="remove-product-file" href="#" data-machine="{{$machine_id}}" data-language="{{$language}}" data-i="{{$i}}" data-type="pdf">remove</a> 
        </div>
    @elseif (isset($pdfErrors['pdf'][0]))
        <div class="error col-lg-3">{{ $pdfErrors['pdf'][0] }}</div>
    @else
        <div class="error col-lg-3"></div>
    @endif
</div>
@endfor