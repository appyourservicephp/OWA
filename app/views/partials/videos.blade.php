@for ($i=1; $i < 4; $i++)
  <?php
   
    $messages = $errors->getMessageBag()->getMessages();
    isset($messages['video'.$i])? $videoErrors = $messages['video'.$i] : $videoErrors = array();
   
  ?>
  <div class="input-field-group">
    <div class="col-lg-3">
      <label class="control-label pull-right" for="lang_{{$languageId}}[note]">Video {{$i}}</label>
    </div>
    <div class="col-lg-6 fileinput fileinput-new" data-provides="fileinput">
      <div class="input-group">
        <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
        <span style="background: white" class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select video</span><span class="fileinput-exists">Change</span>
            <input type="file" name="videos[{{$i}}]"></span>
        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
      </div>
    </div>
    @if (isset($machine_id) && !isset($videoErrors['video'][0]))
      <div class="pdf-info col-lg-3">
        @if (file_exists(public_path().'/machines/'.$machine_id.'/'.$language.'_video_'.$i.'.mp4'))
          Current video {{$i}}: <a href="{{asset('/machines/'.$machine_id.'/'.$language.'_video_'.$i.'.mp4')}}">download</a> | <a class="remove-product-file" href="#" data-machine="{{$machine_id}}" data-language="{{$language}}" data-i="{{$i}}" data-type="video">remove</a>
        @endif
      </div>
    @endif
    <div class="error col-lg-3">{{$videoErrors['video'][0] or null}}</div>
  </div>
@endfor
