@extends('layouts/main')
<!--Web site Title--> 
@section('title'){{ isset($article) ? 'Edit: ' . $article->title : 'New article' }} :: @parent @stop
@section('content')
{{$breadcrumbs}}
<h1>{{isset($article) ? 'Edit: ' . $article->title : Lang::get('news.headline')}}</h1>
<div class="pull-right">
    <div class="pull-right">
        <a class="btn btn-sm btn-info" href="{{'/news/' . $parent->id .'/show' }}">
            <span class="glyphicon glyphicon-backward"></span>
            Back
        </a>
    </div>
</div>

<form class="form-horizontal" enctype="multipart/form-data"
      method="post"
      action="@if(isset($article)){{ URL::to('/news/'. $article->id .'/edit') }}
      @else{{ URL::to('/news/create') }}@endif"
      autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
    <!-- ./ csrf token -->
    @if(isset($parent))
    <input type="hidden" name="id" value="{{{ $parent->id }}}" />
    @endif

    <!-- Tabs Content -->
    <div class="tab-content">
        <!-- General tab -->
        <div class="tab-pane active" id="tab-general">
            <div class="tab-pane active" id="tab-general">
                <?php
                if (count($errors)) {
                    $messages = $errors->getMessageBag()->getMessages();
                    isset($messages['photo']) ? $headerErrors = $messages['photo'] : $titleErrors = array();
                }
                ?>                
                <!-- HEADER PICTURE -->
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <p class="error">
                        {{$headerErrors['header_picture'][0] or null}}
                    </p>
                    @if(isset($article))
                    <?php $headerImageExists = file_exists(public_path() . '/appfiles/news/' . $article->id . '/header_picture.jpg'); ?>
                    @endif
                    <div><label for="header_picture">Header picture* (min 2048x544)</label></div>
                    <div class="fileinput-new thumbnail" style="width: 540px; height: 150px;">
                        @if(!empty($headerImageExists))
                        <img src="{{asset('/appfiles/news/'.$article->id.'/header_picture.jpg')}}" alt="">
                        @endif  
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 540px; max-height: 150px;"></div>
                    <div>
                        <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="header_picture"></span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>

                    </div>

                </div>


                <hr>
                <?php
                if (count($errors)) {
                    $messages = $errors->getMessageBag()->getMessages();
                    isset($messages['title']) ? $titleErrors = $messages['title'] : $titleErrors = array();
                }
                ?>
                <div
                    class="form-group">
                    <div class="col-md-12">
                        <label class="control-label" for="title"> {{
							Lang::get("modal.title") }}</label> <input
                            class="form-control" type="text" name="title" id="title"
                            value="{{{ Input::old('title', isset($article) ? $article->title : null) }}}" />
                        <p class="error">{{$titleErrors['title'][0] or null}}</p>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-12">

                        <label for="parent">{{ Lang::get("modal.parent") }}</label>
                        <select class="form-control" name="parent">
                            @if(isset($roots))
                            @foreach($roots as $root)
                            @if(count($root->subCategories) > 0))
                            <optgroup label="{{ $root->title}}">
                                @foreach($root->subCategories as $subCategory)
                                <option value="{{$subCategory->id}}">{{$subCategory->title}} </option>
                                @endforeach                                
                            </optgroup>
                            @endif
                            @endforeach
                            @elseif(isset($parent))

                            <option value="{{$parent->id}}">{{$parent->title}} </option>

                            @endif

                        </select>

                    </div>
                </div>
                <?php
                if (count($errors)) {
                    $messages = $errors->getMessageBag()->getMessages();
                    isset($messages['content']) ? $contentErrors = $messages['content'] : $contentErrors = array();
                }
                ?>
                <div
                    class="form-group">
                    <div class="col-md-12">
                        <label class="control-label" for="content">{{
							Lang::get("modal.content") }}</label>
                        <textarea class="form-control full-width wysihtml5" name="content"
                                  value="content" rows="10">{{{ Input::old('content', isset($article) ? $article->content : null) }}}</textarea>
                        <p class="error">{{$contentErrors['content'][0] or null}}</p>
                    </div>
                </div>

                <!-- PHOTO -->
                <?php
                if (count($errors)) {
                    $messages = $errors->getMessageBag()->getMessages();
                    isset($messages['photo']) ? $photoErrors = $messages['photo'] : $photoErrors = array();
                }
                ?>
                <hr>
                <h2>Photos</h2>
                <p>One photo with minimal resolution of 458x621px is required</p>
                <p class="error photos-error">
                    {{$photoErrors['pictures'][0] or null}}
                </p>
                @if(isset($article))

                @for ($i=1; $i < 6; $i++)
                <?php $imageExists = file_exists(public_path() . '/appfiles/news/' . $article->id . '/' . $article->id . '_photo_' . $i . '.jpg'); ?>
                <?php $imageThumbnailExists = file_exists(public_path() . '/appfiles/news/' . $article->id . '/' . $article->id . '_photo_' . $i . '_thumbnail.jpg'); ?>

                <div class="fileinput @if($imageExists) fileinput-exists @else fileinput-new @endif" data-provides="fileinput">
                    <div><label for="header_picture">Photo {{$i}}</label></div>
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"></div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 150px;">
                        @if ($imageExists)
                        @if ($imageThumbnailExists)
                        <img src="{{asset('/appfiles/news/'. $article->id.'/' . $article->id . '_photo_' . $i .'_thumbnail.jpg') . '?'}}{{ time() }}">
                        @endif
                        <img class="original-image @if($imageThumbnailExists) hidden @endif" data-machine-id="{{$article->id}}" data-type ="news" data-image-name="{{$article->id . '_photo_'.$i.'.jpg'}}" src="{{asset('/appfiles/news/'.$article->id.'/'. $article->id . '_photo_'.$i.'.jpg' . '?')}}{{ time() }}">
                        @endif
                    </div>
                    <div>
                        <span class="btn btn-default btn-file change-image"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="pictures[{{$i}}]"></span>
                        <a href="#" class="remove-image btn btn-default fileinput-exists" data-dismiss="fileinput" data-type="news" data-machine-id="{{$article->id}}" data-image-name="{{$article->id . '_photo_'.$i.'.jpg'}}">Remove</a>
                        <br/>
                        <div class="show-thumbnail">Thumbnail <span class="glyphicon @if($imageThumbnailExists) glyphicon-ok @else glyphicon-remove @endif"></span></div>
                        @if ($imageThumbnailExists)
                        <a class="show-crop-modal btn btn-default" >Change thumbnail</a><br/>
                        <a class="remove-image btn btn-default" data-machine-id="{{$article->id}}" data-type="news" data-image-name="{{$article->id . '_photo_'.$i.'_thumbnail.jpg'}}">Remove thumbnail</a>
                        @else
                        <a class="show-crop-modal btn btn-default" @if(!$imageExists) style="visibility:hidden" @endif >Create thumbnail</a><br/>
                        <div class="thumbnail-warning" style="visibility:hidden">To be able to create thumbnail,<br/> you will first need to save this machine.</div>
                        @endif
                    </div>
                </div>
                @if ($i==5)
                <hr>
                @endif
                @endfor
                <hr>
                @else
                <hr>

                @for ($i=1; $i < 6; $i++)
                <div class="fileinput fileinput-new photos" data-provides="fileinput">
                    <div><label for="header_picture">Photo {{$i}}</label></div>
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 150px;"></div>
                    <div>
                        <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="pictures[{{$i}}]"></span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
                @if ($i==5)
                <hr>
                @endif
                @endfor

                <hr>
                @endif
                <!-- PDF -->

                @for ($i=1; $i < 3; $i++)
                <?php
                if (count($errors)) {
                    $messages = $errors->getMessageBag()->getMessages();
                    isset($messages['pdf' . $i]) ? $pdfErrors = $messages['pdf' . $i] : $pdfErrors = array();
                }
                ?>
                <div class="input-field-group pdf-input">
                    <div class="col-lg-3">
                        <label class="control-label pull-right" for="pdf">Pdf {{ $i}}</label>
                    </div>
                    <div class="col-lg-6 fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group">
                            <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                            <span style="background: white" class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select pdf</span><span class="fileinput-exists">Change</span><input id="pdf" type="file" name="pdf[{{$i}}]"></span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>

                    @if ( isset($article) && file_exists(public_path() . '/appfiles/news/' . $article->id . '/' . $article->id . '_pdf_' . $i . '.pdf'))

                    <div class="pdf-info col-lg-3">

                        Current pdf: <a href="{{asset('/appfiles/news/' . $article->id . '/' . $article->id . '_pdf_' . $i . '.pdf')}}">download</a> | <a class="remove-news-file" href="#" data-id="{{ $article->id }}" data-i="{{$i}}" data-type="pdf" >remove</a> 
                    </div>
                    @endif
                    <div class="error col-lg-3">{{$pdfErrors['pdf'][0] or null}}</div>
                </div>
                @endfor
                <hr>

                <!-- VIDEO -->

                @for ($i=1; $i < 3; $i++)
                <?php
                if (count($errors)) {
                    $messages = $errors->getMessageBag()->getMessages();
                    isset($messages['video' . $i]) ? $videoErrors = $messages['video' . $i] : $videoErrors = array();
                }
                ?>
                <div class="input-field-group">
                    <div class="col-lg-3">
                        <label class="control-label pull-right" for="video">Video {{$i}}</label>
                    </div>
                    <div class="col-lg-6 fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group">
                            <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                            <span style="background: white" class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select video</span><span class="fileinput-exists">Change</span>
                                <input type="file" name="video[{{$i}}]"></span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                    @if (isset($article) && !isset($videoErrors['video'][0]))
                    <div class="pdf-info col-lg-3">

                        @if (file_exists(public_path().'/appfiles/news/'. $article->id . '/'. $article->id . '_video_' . $i . '.mp4'))

                        Current video {{$i}}: <a href="{{asset('/appfiles/news/'. $article->id . '/'. $article->id . '_video_' . $i . '.mp4')}}">download</a> | <a class="remove-news-file" href="#" data-id="{{ $article->id}}" data-i="{{$i}}" data-type="video">remove</a>
                        @endif
                    </div>
                    @endif
                    <div class="error col-lg-3">{{$videoErrors['video'][0] or null}}</div>
                </div>
                @endfor



                <!-- ./ general tab -->
            </div>
            <!-- ./ tabs content -->

            <!-- Form Actions -->

            <div class="form-group">
                <div class="col-md-12">

                    <button type="submit" class="btn btn-sm btn-success">
                        <span class="glyphicon glyphicon-ok-circle"></span> 
                        @if(isset($article)) 
                        {{ Lang::get("news.edit") }} 
                        @else 
                        {{Lang::get("news.create") }} 
                        @endif
                    </button>
                    @if(Auth::user()->hasRole('super_admin') && isset($categories))

                    <div class="pull-right">
                        <div class="pull-right">
                            <a class="btn btn-sm btn-danger delete" href="{{'/news/'. $article->id . '/delete'}}">
                                <span class="glyphicon glyphicon-remove"></span>
                                {{ Lang::get('news.delete') }}
                            </a>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <!-- ./ form actions -->

            </form>
            <!-- Modal -->
            <div class="modal fade" id="cropImageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Create thumbnail</h4>
                        </div>
                        <div class="modal-body"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="crop-image btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            @stop