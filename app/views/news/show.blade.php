@extends('layouts/main')
 <!--Web site Title--> 
@section('title') {{{ $parent->title }}} :: @parent @stop
@section('content')

{{$breadcrumbs}}

<h1>Article: {{$parent->title}}</h1>
@if(Auth::user()->hasRole('super_admin'))
<div class="pull-right">
    <div class="pull-right">
        <a class="btn btn-sm btn-danger deleteAll" href="{{'/categories/'. $parent->id . '/delete'}}">
            <span class="glyphicon glyphicon-remove"></span>
            Delete {{ $parent->title }} article
        </a>
    </div>
</div>
@endif
<p>
    <a href="/news/{{$parent->id}}/new" class="btn btn-success">Add new article</a>
</p>

<table class="table table-bordered">
    <thead>
        
        <tr>
            <th>Model</th>
            <th>Title</th>
            <th>Created at</th>
            <th>Controls</th>
        </tr>
        
    </thead>
    @foreach ($articles as $article)
    <tbody id="sortable">
        
         
         <tr class="machine-table-row" data-id="{{$article->id}}">
            
           
                <td class="handle"><span class="move-icon glyphicon glyphicon-resize-vertical"></span></td>
                <td>{{$article->title}}</td>
                <td>{{$article->created_at}}</td>
            
            
            <td width="15%">
                <a href="/news/{{$article->id}}/edit" class="btn btn-primary">Adjust</a>
                <a href="/news/{{$article->id}}/delete" class="btn btn-danger pull-right" onclick="return confirm('Are you sure you want to delete this article?')">Remove</a>
            </td>
        </tr>
        
        
    </tbody>
    @endforeach
</table>

@stop