@extends('layouts/main')
<!--Web site Title--> 
@section('title') Logo :: @parent @stop
@section('content')

<div class="page-header">
    <h3>
        {{$title}}
    </h3>
</div>
@if($errors->all())
<div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4>Error</h4>
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="row">
    <form class="form-horizontal" enctype="multipart/form-data"
          method="post" action="{{URL::to('logo')}}">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <!-- ./ csrf token -->
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Title</label>
            <div class="col-sm-10">
                <input type="text" name="title" class="form-control" id="title" placeholder="Title">
            </div>
        </div>
        <div class="form-group">
            <label for="shortcode" class="col-sm-2 control-label">Shortcode</label>
            <div class="col-sm-10">
                <input type="text" name="shortcode" class="form-control" id="shortcode" placeholder="shortcode">
            </div>
        </div>
        <div class="form-group ">
            <label class="col-sm-2 control-label" for="picture">Picture</label>
            <div class="col-sm-10">
                <input type="file" name="picture" class="uploader" id="picture" value="Upload" />
            </div>
        </div>

        <button type="submit" class="btn btn-default">Upload</button>
    </form>

    <hr>
    <table id="logos" class="table table-hover table-condensed">
        <thead>
            <tr>
                <th class="col-md-3">Title</th>
                <th class="col-md-3">Shortcode</th>
                <th class="col-md-3">Name</th>
                <th class="col-md-3">Actions</th>

            </tr>
        </thead>
    </table>

</div>
@endsection
