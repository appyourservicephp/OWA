@extends('layouts/main')
<!--Web site Title--> 
@section('title') {{$title}} :: @parent @stop
@section('content')

{{$breadcrumbs}}
@include('notifications')



<h1>{{$title}}</h1>
<hr>

@foreach($parents as $key => $parent)

@if(count($parent)> 0)


        <ul class="list-group">
            
            <?php $projects = Project::where('category_id', $parent['id'])->get(); ?>
            <li class="list-group-item"><a href="{{'/projects/'. $parent['id']. '/show'}}">{{$parent['title']}} <span class="badge pull-right">{{ count($projects)}}</span></a>
            
        </ul>


@else

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{{$parent['title']}}</h3>
    </div>
    <div class="panel-body">
        <small>There are no projects for this category</small>
    </div>
</div>
@endif
@endforeach

@stop