@extends('layouts/main')
<!--Web site Title--> 
@section('title') {{Category::find($category_id)->title}} edit :: @parent @stop
@section('content')

{{$breadcrumbs}}

<h1>{{$productTitle->name}} edit</h1>
<form id="edit-machine" action="/edit-product" method="post" enctype="multipart/form-data" class="navbar-form navbar-left">

    <?php
    if (count($errors)) {
        $messages = $errors->getMessageBag()->getMessages();
        isset($messages['photo']) ? $photoErrors = $messages['photo'] : $photoErrors = array();
    }
    ?>
    <div style="margin-right: 50px" class="fileinput fileinput-new" data-provides="fileinput">
        <p class="error">
            {{$photoErrors['main_picture'][0] or null}}
        </p>
        <?php $imageExists = file_exists(public_path() . '/machines/' . $machine_id . '/main_picture.jpg'); ?>
        <?php $imageThumbnailExists = file_exists(public_path() . '/machines/' . $machine_id . '/main_picture_thumbnail.jpg'); ?> 
        <div><label for="main_picture">Main picture* (min 458x621)</label></div>
        <div class="fileinput-new thumbnail" style="width: 117px; height: 150px;">
            @if ($imageExists)
            @if ($imageThumbnailExists)
            <img src="{{asset('/machines/'.$machine_id.'/main_picture_thumbnail.jpg') . '?'}}{{ time() }}">
            @endif
            <img class="original-image @if($imageThumbnailExists) hidden @endif" data-type="products" data-machine-id="{{$machine_id}}" data-image-name="{{'main_picture.jpg'}}"  src="{{asset('/machines/'.$machine_id.'/main_picture.jpg' . '?')}}{{ time() }}" alt="">
            @endif


        </div>
        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 117px; max-height: 150px;"></div>
        <div>
            <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="main_picture"></span>
            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>

        <br/>
        <div class="show-thumbnail">Thumbnail <span class="glyphicon @if($imageThumbnailExists) glyphicon-ok @else glyphicon-remove @endif"></span></div>
        @if ($imageThumbnailExists)
        <a class="show-crop-modal btn btn-default" >Change thumbnail</a><br/>
        <a class="remove-image btn btn-default" data-machine-id="{{$machine_id}}" data-image-name="{{'main_picture_thumbnail.jpg'}}">Remove thumbnail</a>
        @else
        <a class="show-crop-modal btn btn-default" @if(!$imageExists) style="visibility:hidden" @endif >Create thumbnail</a><br/>
        <div class="thumbnail-warning" style="visibility:hidden">To be able to create thumbnail,<br/> you will first need to save this machine.</div>
        @endif

    </div>
    <!-- HEADER PICTURE -->
    <div class="fileinput fileinput-new" data-provides="fileinput">
        <p class="error">
            {{$photoErrors['header_picture'][0] or null}}
        </p>
        <?php $imageExists = file_exists(public_path() . '/machines/' . $machine_id . '/header_picture.jpg'); ?>
        <div><label for="header_picture">Header picture* (min 2048x544)</label></div>
        <div class="fileinput-new thumbnail" style="width: 540px; height: 150px;">
            <img src="{{asset('/machines/'.$machine_id.'/header_picture.jpg' . '?')}}{{ time() }}" alt="">
        </div>
        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 540px; max-height: 150px;"></div>
        <div>
            <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="header_picture"></span>
            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>


        </div>



    </div>


    <hr>
    <h2>Photo page</h2>
    <p>One photo with minimal resolution of 458x621px is required</p>
    <p class="error photos-error">
        {{$photoErrors['pictures'][0] or null}}
    </p>
    @for ($i=1; $i < 11; $i++)
    <?php $imageExists = file_exists(public_path() . '/machines/' . $machine_id . '/photo_' . $i . '.jpg'); ?>
    <?php $imageThumbnailExists = file_exists(public_path() . '/machines/' . $machine_id . '/photo_' . $i . '_thumbnail.jpg'); ?>
    <div class="fileinput @if($imageExists) fileinput-exists @else fileinput-new @endif" data-provides="fileinput">
        <div><label for="header_picture">Photo {{$i}}</label></div>
        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"></div>
        <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 150px;">
            @if ($imageExists)
            @if ($imageThumbnailExists)
            <img src="{{asset('/machines/'.$machine_id.'/photo_'.$i.'_thumbnail.jpg') . '?'}}{{ time() }}">
            @endif
            <img class="original-image @if($imageThumbnailExists) hidden @endif" data-type="products" data-machine-id="{{$machine_id}}" data-image-name="{{'photo_'.$i.'.jpg'}}" src="{{asset('/machines/'.$machine_id.'/photo_'.$i.'.jpg' . '?')}}{{ time() }}">
            @endif
        </div>
        <div>
            <span class="btn btn-default btn-file change-image"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="pictures[{{$i}}]"></span>
            <a href="#" class="remove-image btn btn-default fileinput-exists" data-dismiss="fileinput" data-machine-id="{{$machine_id}}" data-image-name="{{'photo_'.$i.'.jpg'}}">Remove</a>
            <br/>
            <div class="show-thumbnail">Thumbnail <span class="glyphicon @if($imageThumbnailExists) glyphicon-ok @else glyphicon-remove @endif"></span></div>
            @if ($imageThumbnailExists)
            <a class="show-crop-modal btn btn-default" >Change thumbnail</a><br/>
            <a class="remove-image btn btn-default" data-machine-id="{{$machine_id}}" data-image-name="{{'photo_'.$i.'_thumbnail.jpg'}}">Remove thumbnail</a>
            @else
            <a class="show-crop-modal btn btn-default" @if(!$imageExists) style="visibility:hidden" @endif >Create thumbnail</a><br/>
            <div class="thumbnail-warning" style="visibility:hidden">To be able to create thumbnail,<br/> you will first need to save this machine.</div>
            @endif
        </div>
    </div>
    @if ($i==5)
    <hr>
    @endif
    @endfor
    <!-- Nav tabs -->


    <?php
//    if (count($errors)) {
//         $msg = $errors->getMessageBag()->getMessages();
//        isset($msg['lang_' . $language->id]) ? $error = 'error' : $error = '';
//    }
//    
    ?>


    <input id="catHidden" type="hidden" name="category_id" value="{{$category_id}}">
    <input type="hidden" name="machine_id" value="{{$machine_id}}">

    <!-- Tab panes -->
    <div class="tab-content">
        @foreach ($languages as $index => $language)
        <?php $languageId = $language->id; ?>
        <div class="tab-pane @if($index == 0) active @endif" id="<?php echo $language->name; ?>">
            <?php
            if (count($errors))
                $msg = $errors->getMessageBag()->getMessages();
            isset($msg['lang_' . $languageId]) ? $messages = $msg['lang_' . $languageId] : $messages = array();
            if (Input::old('lang_' . $languageId)) {
                $input = Input::old('lang_' . $languageId);
            } else {
                $input = $data[$languageId];
            }
            ?>
            <div class="form-group col-lg-12">
                <fieldset>
                    <input class="language" type="hidden" data-required="{{in_array($languageId,$requiredLanguageIds)}}" name="lang_{{$languageId}}[language_id]" value="{{$languageId}}">

                    <h2>About us</h2>
                    <hr/>
                    <div class="input-field-group" style="margin-bottom:20px;">
                        <div class="col-lg-3">
                            <label class="control-label pull-right" for="lang_{{$language->id}}[name]">Title</label>
                        </div>
                        <div class="col-lg-6">
                            <input name="lang_{{$language->id}}[name]" value="{{$input['name'] or ''}}" type="text" class="form-control input-md" id="lang_{{$language->id}}_name" >
                        </div>
                        <div class="error_lang_{{$language->id}}_name error col-lg-3"></div>
                    </div>
                     <!-- CONTENT -->
                    <div  class="input-field-group">
                        <div class="col-md-12" >
                            <label class="control-label" for="content">{{
							Lang::get("modal.content") }}</label>
                            <textarea  class="form-control full-width wysihtml5" name="lang_{{$language->id}}[intro]"
                                      value="content" rows="10">{{ $input['intro'] or ''}}</textarea>
                        </div>
                    </div>
                    
                     
                             

                    <h2>Videos</h2>
                    <hr/>
                    @include('partials.videos', array('languageId' => $languageId, 'machine_id' => $machine_id, 'language' => $language->slug, 'messages' => isset($messages) ? $messages : NULL))
                    <h2>PDF</h2>
                    <hr/>
                    @include('partials.pdfs', array('languageId' => $languageId, 'machine_id' => $machine_id, 'language' => $language->slug, 'messages' => $messages))
                </fieldset>
            </div>
        </div>
        @endforeach
    </div>

    <div class="form-submit col-md-2 col-md-offset-5">
        <button type="submit" class="btn btn-success pull-right">Save</button>
        <a class="btn btn-danger pull-right" href="/products/{{$category_id}}/show">Cancel</a>
    </div>
</form>

<!-- Modal -->
<div class="modal fade" id="cropImageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Create thumbnail</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="crop-image btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>

@stop