@extends('layouts/main')
<!--Web site Title--> 
@section('title') About us :: @parent @stop
@section('content')

{{$breadcrumbs}}

<!-- MAIN PICTURE -->
<form class="form-horizontal" enctype="multipart/form-data"
      method="post"
      action="{{ URL::to('/about-us/image') }}"
      autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
    <!-- ./ csrf token -->
    <input type="hidden" name="id" value="{{$category->id}}" />
    <div style="margin-right: 50px" class="fileinput fileinput-new {{{ $errors->has('image') ? 'has-error' : '' }}}" data-provides="fileinput">
        <p class="error">
            @if(isset($category))
            <?php $imageExists = file_exists(public_path() . '/appfiles/categories/' . $category->id . '/main_picture_thumbnail.jpg'); ?>
            @endif
            {{$errors->first('image', '<label class="control-label">:message</label>')}}
        </p>
        <div><label  for="image">Main picture* (min 409x1408)</label></div>                

        <div class="fileinput-new thumbnail thumbnailCat" >

            @if(!empty($imageExists))
            <img id="img" src="{{'/appfiles/categories/' . $category->id . '/main_picture_thumbnail.jpg'}}" alt="" />
            @endif
        </div>
        <div class="fileinput-preview fileinput-exists thumbnail thumbnailCat"></div>
        <div>
            <span class="btn btn-default btn-file"><span class="fileinput-new">{{ !empty($imageExists) ? 'Change image' : 'Select image' }}</span><span class="fileinput-exists">Change</span><input type="file" name="image"></span>
            @if(!empty($imageExists))
            <a class="btn btn-default btn-danger remove-category-pdf" href="#" data-type="img" data-id="{{ $category->id }}">remove</a>
            @endif
        </div>
        
        <button class="btn btn-primary" type="submit">Upload</button>
        
    </div>
    
</form>
<hr>

<!-- END MAIN IMAGE -->

<h1>About us product management</h1>

<?php $about = Product::where('category_id', '5')->first(); ?>
@if(Request::is('about-us') && count($about) > 0)
<p style="margin-bottom: 30px"></p>
@else
@if(Auth::user()->hasRole('super_admin') && $category_id !== '5')
<div class="pull-right">
    <div class="pull-right">
        <a class="btn btn-sm btn-danger deleteAll" href="{{'/categories/'. $category_id . '/delete'}}">
            <span class="glyphicon glyphicon-remove"></span>
            Delete {{ $category->title }} category
        </a>
    </div>
</div>
@endif
<p>
    <a href="/about-us/{{$category_id}}/new" class="btn btn-success">Add product</a>
</p>
@endif
<table class="table table-bordered">
    <thead>
        @if($category->slug == 'lce')
        <tr>
            <th>&nbsp;</th>
            <th width="75%"></th>
            <th width="15%">Controls</th>
        </tr>
        @else
        <tr>
            <th>Model</th>
            <th>Title</th>
            <th>Created at</th>
            <th>Controls</th>
        </tr>
        @endif
    </thead>
    <tbody id="sortable">
        @foreach ($machines as $machine)

        <tr class="machine-table-row" data-id="{{$machine->id}}">
            <?php $spec = $machine->machineLanguages->first(); ?>
            <?php if ($spec): ?>
                <td class="handle"><span class="move-icon glyphicon glyphicon-resize-vertical"></span></td>
                <td>{{$spec->name}}</td>
                <td>{{$spec->created_at}}</td>
            <?php endif; ?>

            <td width="15%">

                <a href="/about-us/{{$category_id}}/edit/{{$machine->id}}" class="btn btn-primary">Adjust</a>
                <!--<a href="/delete/{{$machine->id}}" class="btn btn-danger pull-right" onclick="return confirm('Are you sure you want to delete this product?')">Remove</a>-->


            </td>
        </tr>

        @endforeach
    </tbody>
</table>
@stop