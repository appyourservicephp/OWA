@extends('layouts/main')
<!--Web site Title--> 
@section('title') New product for About us :: @parent @stop
@section('content')

{{$breadcrumbs}}

<h1>About us product new</h1>
<form id="add-machine" action="/add-product" method="post" enctype="multipart/form-data" class="navbar-form navbar-left">


    <?php
    if (count($errors)) {
        $messages = $errors->getMessageBag()->getMessages();
        isset($messages['photo']) ? $photoErrors = $messages['photo'] : $photoErrors = array();
    }
    ?>
    <div style="margin-right: 50px" class="fileinput fileinput-new" data-provides="fileinput">
        <p class="error">
            {{$photoErrors['main_picture'][0] or null}}
        </p>
        <div><label for="main_picture">Main picture* (min 458x621)</label></div>
        <div class="fileinput-new thumbnail" style="width: 117px; height: 150px;">
        </div>
        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 117px; max-height: 150px;"></div>
        <div>
            <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="main_picture"></span>
            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
    <!-- HEADER PICTURE -->
    <div class="fileinput fileinput-new" data-provides="fileinput">
        <p class="error">
            {{$photoErrors['header_picture'][0] or null}}
        </p>
        <div><label for="header_picture">Header picture* (min 2048x544)</label></div>
        <div class="fileinput-new thumbnail" style="width: 540px; height: 150px;">
        </div>
        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 540px; max-height: 150px;"></div>
        <div>
            <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="header_picture"></span>
            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
    <hr>

    <h2>Photo page</h2>
    <p>One photo with minimal resolution of 458x621px is required</p>
    <p class="error photos-error">
        {{$photoErrors['pictures'][0] or null}}
    </p>
    @for ($i=1; $i < 11; $i++)
    <div class="fileinput fileinput-new photos" data-provides="fileinput">
        <div><label for="header_picture">Photo {{$i}}</label></div>
        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
        </div>
        <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 150px;"></div>
        <div>
            <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="pictures[{{$i}}]"></span>
            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
    @if ($i==5)
    <hr>
    @endif
    @endfor

    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        @foreach ($languages as $index => $language)
        <?php
        $msg = $errors->getMessageBag()->getMessages();
        isset($msg['lang_' . $language->id]) ? $error = 'error' : $error = '';
        ?>
        <li @if($index == 0) class="active" @endif><a id="{{$language->id}}" href="#{{$language->name}}" data-toggle="tab">{{ ($language->required)?$language->name.'*':$language->name}} @if($error == 'error') <span class="error glyphicon glyphicon-ban-circle"></span> @endif</a></li>
        @endforeach
    </ul>

    <input id="catHidden" type="hidden" name="category_id" value="{{$category_id}}">
    <!-- Tab panes -->
    <div class="tab-content">
        @foreach ($languages as $index => $language)
        <div class="tab-pane @if($index == 0) active @endif" id="<?php echo $language->name; ?>"><br>
            <?php
            if (count($errors)) {
                $msg = $errors->getMessageBag()->getMessages();
                isset($msg['lang_' . $language->id]) ? $messages = $msg['lang_' . $language->id] : $messages = array();
                $input = Input::old('lang_' . $language->id);
            }
            ?>
            <div class="form-group col-lg-12">
                <fieldset>
                    <input class="language" type="hidden" data-required="{{in_array($language->id,$requiredLanguageIds)}}" name="lang_{{$language->id}}[language_id]" value="{{$language->id}}">
                    <h2>About us</h2>
                    <hr/>
                    <!-- NAME -->
                    <div class="input-field-group" style="margin-bottom:20px;">
                        <div class="col-lg-3">
                            <label class="control-label pull-right" for="lang_{{$language->id}}[name]">Title</label>
                        </div>
                        <div class="col-lg-6">
                            <input name="lang_{{$language->id}}[name]" value="{{$input['name'] or ''}}" type="text" class="form-control input-md" id="lang_{{$language->id}}_name" >
                        </div>
                        <div class="error_lang_{{$language->id}}_name error col-lg-3"></div>
                    </div>
                    <!-- CONTENT -->
                    <div  class="input-field-group">
                        <div class="col-md-12" >
                            <label class="control-label" for="content">{{
							Lang::get("modal.content") }}</label>
                            <textarea  class="form-control full-width wysihtml5" name="lang_{{$language->id}}[intro]"
                                      value="content" rows="10"></textarea>
                        </div>
                    </div>


                    <h2>Videos</h2>
                    <hr/>
                    @include('partials.videos', array('languageId' => $language->id, 'language' => $language->slug, 'messages' => isset($messages) ? $messages : NULL))
                    <h2>PDF</h2>
                    <hr/>
                    @include('partials.pdfs', array('languageId' => $language->id, 'requiredLanguageIds' => $requiredLanguageIds, 'messages' => isset($messages) ? $messages : NULL))
                </fieldset>
            </div>
        </div>
        @endforeach
    </div>

    <div class="form-submit col-md-2 col-md-offset-5">
        <button type="submit" class="btn btn-success pull-right">Save</button>
        <a class="btn btn-danger pull-right" href="/categories/{{$category_id}}">Cancel</a>
    </div>
</form>
@stop