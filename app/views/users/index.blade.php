@extends('layouts/main')
<!--Web site Title--> 
@section('title') {{ $title}} :: @parent @stop
@section('content')
{{$breadcrumbs}}
<div class="page-header">
    <h3>
        {{$title}}
        @if(Auth::user()->hasRole('super_admin'))
        <div class="pull-right">
            <div class="pull-right">
                <a class="btn btn-sm btn-info" href="/user/create">
                    <span class="glyphicon glyphicon-user"></span>
                    New
                </a>
            </div>
        </div>
        @endif
    </h3>

</div>
@if($errors->all())
<div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4>Error</h4>
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="row">
    <table id="users" class="table table-hover table-condensed">
        <thead>
            <tr>
                <th class="col-md-3">Username</th>
                <th class="col-md-3">Email</th>
                <th class="col-md-3">Confirmed</th>
            <th class="col-md-3">Actions</th>
        </tr>
    </thead>
</table>


</div>

@endsection
