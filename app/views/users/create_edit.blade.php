@extends('layouts/main')
<!--Web site Title--> 
@section('title') {{ $title}} :: @parent @stop
@section('content')
{{$breadcrumbs}}
<div class="page-header">
    <h3>
        {{$title}}
        <div class="pull-right">
            <div class="pull-right">
                <a class="btn btn-sm btn-info" href="/users/">
                    <span class="glyphicon glyphicon-backward"></span>
                    Back
                </a>
            </div>
        </div>
    </h3>

</div>

<div class="row">
   
    <form class="form-horizontal" method="post"
          action="@if (isset($user)){{ URL::to('/user/' . $user->id . '/edit') }} @else {{ URL::to('/user/create') }} @endif"
          autocomplete="off">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
               
               
                <div class="col-md-12">
                    <div class="form-group {{{ $errors->has('username') ? 'has-error' : '' }}}">
                        <label class="col-md-2 control-label" for="username">{{
						Lang::get('users.username') }}</label>
                        <div class="col-md-10">
                            <input class="form-control" type="username" tabindex="4"
                                   placeholder="{{ Lang::get('users.username') }}" name="username"
                                   id="username"
                                   value="{{{ Input::old('username', isset($user) ? $user->username : null) }}}" />
                            {{ $errors->first('username', '<label class="control-label"
                                                                   for="username">:message</label>')}}
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group {{{ $errors->has('email') ? 'has-error' : '' }}}">
                        <label class="col-md-2 control-label" for="email">{{
						Lang::get('users.email') }}</label>
                        <div class="col-md-10">
                            <input class="form-control" type="email" tabindex="4"
                                   placeholder="{{ Lang::get('users.email') }}" name="email"
                                   id="email"
                                   value="{{{ Input::old('email', isset($user) ? $user->email : null) }}}" />
                            {{ $errors->first('email', '<label class="control-label"
                                                                for="email">:message</label>')}}
                        </div>
                    </div>
                </div>
               
                <div class="col-md-12">
                    <div class="form-group {{{ $errors->has('password') ? 'has-error' : '' }}}">
                        <label class="col-md-2 control-label" for="password">{{
						Lang::get('users.password') }}</label>
                        <div class="col-md-10">
                            <input class="form-control" tabindex="5"
                                   placeholder="{{ Lang::get('users.password') }}"
                                   type="password" name="password" id="password" value="" />
                            {{$errors->first('password', '<label class="control-label"
                                                                  for="password">:message</label>')}}
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group {{{ $errors->has('password_confirmation') ? 'has-error' : '' }}}">
                        <label class="col-md-2 control-label" for="password_confirmation">{{
						Lang::get('users.password_confirmation') }}</label>
                        <div class="col-md-10">
                            <input class="form-control" type="password" tabindex="6"
                                   placeholder="{{ Lang::get('users.password_confirmation') }}"
                                   name="password_confirmation" id="password_confirmation" value="" />
                            {{$errors->first('password_confirmation', '<label
                                class="control-label" for="password_confirmation">:message</label>')}}
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-">
                        <label class="col-md-2 control-label" for="confirm">{{
						Lang::get('users.activate_user') }}</label>
                        <div class="col-md-6">
                            <select class="form-control" name="confirmed" id="confirmed">
                                <option value="1" {{{ ((isset($user) && $user->confirmed == 1)? '
                                    selected="selected"' : '') }}}>{{{ Lang::get('users.yes')
                                    }}}</option>
                                <option value="0" {{{ ((isset($user) && $user->confirmed == 0) ?
                                    ' selected="selected"' : '') }}}>{{{ Lang::get('users.no')
                                    }}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <br>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="roles">{{
						Lang::get('users.roles') }}</label>
                        <div class="col-md-6">
                            <select name="roles[]" id="roles" multiple style="width: 100%;">
                                @foreach ($roles as $role)
                                <option value="{{{ $role->id }}}" {{{ ( array_search($role->id,
                                        $selectedRoles) !== false && array_search($role->id,
                                        $selectedRoles) >= 0 ? ' selected="selected"' : '') }}}>{{{
								$role->name }}}</option> @endforeach
                            </select> <span class="help-block"> {{
							Lang::get('users.roles_info') }} </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">


                <button type="submit" class="btn btn-sm btn-success">
                    <span class="glyphicon glyphicon-ok-circle"></span> 
                    @if	(isset($user))
                    {{ Lang::get("users.edit") }}
                    @else
                    {{Lang::get("users.create") }} 
                    @endif
                </button>
            </div>
        </div>
    </form>   


</div>

@endsection
