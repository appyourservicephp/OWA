<!DOCTYPE HTML>
<html>
	<head>
		<title>0WA :: 404</title>
		
		<link href="{{ asset('css/404.css') }}" rel="stylesheet" type="text/css"  media="all" />
	</head>
	<body>
		<!--start-wrap--->
		<div class="wrap">
			<!---start-header---->
				<div class="header">
					<div class="logo">
						<h1><a href="#">Woopsie Daisy!</a></h1>
					</div>
				</div>
			<!---End-header---->
			<!--start-content------>
			<div class="content">
				<img src="{{asset('images/error-img.png')}}" title="error" />
				<p><span><label>O</label>hh.....</span>You Requested the page that is no longer There.</p>
				<a href="{{ url('/categories') }}">Back To Home</a>
				<div class="copy-right">
					<p>{{ date('Y') }} All rights Reserved | OWA</p>
				</div>
   			</div>
			<!--End-Cotent------>
		</div>
		<!--End-wrap--->
	</body>
</html>
