@extends('layouts/main')
@section('title') @if(isset($categories)) Edit :: {{ $categories->title}} @else New category @endif @parent @stop
@section('content')

{{$breadcrumbs}}
<?php
if (isset($parents)) {
    $results = array();
    foreach ($parents as $parent) {
        $v = array_push($results, $parent->id);
        foreach ($parent->subCategories as $key => $child) {
            array_push($results, $child->id);
            foreach ($child->subCategories as $key => $p) {
                array_push($results, $p->id);
            }
        }
    }
}
?>
<h1>@if(isset($categories))
    Edit {{ $categories->title }}
    @else
    {{Lang::get('modal.headline')}}
    @endif
</h1>
<div class="pull-right">
    <div class="pull-right">
        <a class="btn btn-sm btn-info" href="/categories">
            <span class="glyphicon glyphicon-backward"></span>
            Back
        </a>
    </div>
</div>
<form class="form-horizontal" enctype="multipart/form-data"
      method="post"
      action="@if(isset($categories)){{ URL::to('/categories/'. $categories->id .'/edit') }}
      @else{{ URL::to('/categories/create') }}@endif"
      autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
    <!-- ./ csrf token -->
    <!-- Tabs Content -->
    <div class="tab-content">
        <!-- General tab -->
        <div class="tab-pane active" id="tab-general">
            <div class="tab-pane active" id="tab-general">
                
                
                 <!-- PICTURE -->

                <div style="margin-right: 50px" class="fileinput fileinput-new {{{ $errors->has('image') ? 'has-error' : '' }}}" data-provides="fileinput">
                    <p class="error">
                        @if(isset($categories))
                        <?php $imageExists = file_exists(public_path() . '/appfiles/categories/' . $categories->id . '/main_picture_thumbnail.jpg'); ?>
                        @endif
                        {{$errors->first('image', '<label class="control-label">:message</label>')}}
                    </p>
                    <div><label class="image" for="image"></label></div>                

                    <div class="fileinput-new thumbnail thumbnailCat" >

                        @if(!empty($imageExists))
                        <img id="img" src="{{'/appfiles/categories/' . $categories->id . '/main_picture_thumbnail.jpg'}}" alt="" />
                        @endif
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail thumbnailCat"></div>
                    <div>
                        <span class="btn btn-default btn-file"><span class="fileinput-new">{{ !empty($imageExists) ? 'Change image' : 'Select image' }}</span><span class="fileinput-exists">Change</span><input type="file" name="image"></span>
                        @if(!empty($imageExists))
                        <a class="btn btn-default btn-danger remove-category-pdf" href="#" data-type="img" data-id="{{ $categories->id }}">remove</a>
                        @endif
                    </div>
                </div>
                 <hr>
                <!-- TITLE -->
                
                <div
                    class="form-group {{{ $errors->has('title') ? 'has-error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="title"> {{
							Lang::get("modal.title") }}</label> <input
                            class="form-control" type="text" name="title" id="title"
                            value="{{{ Input::old('title', isset($categories) ? $categories->title : null) }}}" />
                        {{$errors->first('title', '<label class="control-label">:message</label>')}}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-12">

                        <label for="parent">{{ Lang::get("modal.parent") }}</label>

                        <select class="form-control" id="parent" name="parent">
                             @if(Auth::user()->hasRole('super_admin'))
                                @if(isset($roots))                     
                                    @if (count($roots) > 0) 
                                        @foreach($roots as $root)
                                            @if($root->id !== '5')
                                                @if(count($root->subCategories) > 0 && $root->id !== '5')                                               
                                                    <option value="{{ $root->id }}"> {{ $root->title }} </option>                                                 
                                                    @if($root->id !== '3'  && $root->id !== '4' )
                                                        @foreach($root->subCategories as $subCategory)                                               
                                                            <option value="{{ $subCategory->id }}"> - {{ $subCategory->title }} </option>                                                  
                                                        @endforeach
                                                    @endif 
                                                @else
                                                    <option value="{{ $root->id }}"> {{ $root->title }} </option>
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif
                                @elseif(isset($parentId))                            
                                    <option  value="{{ $parentId->id }}"> {{ $parentId->title }} </option>                            
                                @endif
                            @else
                             @if(isset($roots))                     
                                    @if (count($roots) > 0) 
                                        @foreach($roots as $root)
                                            @if($root->id !== '5')
                                                @if(count($root->subCategories) > 0 && $root->id !== '5')                                               
                                                    <optgroup label="{{ $root->title }}">                                               
                                                    @if($root->id !== '3'  && $root->id !== '4' )
                                                        @foreach($root->subCategories as $subCategory)                                               
                                                            <option value="{{ $subCategory->id }}"> - {{ $subCategory->title }} </option>                                                  
                                                        @endforeach
                                                    @endif
                                                     </optgroup>
                                                @else
                                                    <option value="{{ $root->id }}"> {{ $root->title }} </option>
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif
                                @elseif(isset($parentId))                            
                                    <option  value="{{ $parentId->id }}"> {{ $parentId->title }} </option>                            
                                @endif
                            @endif
                        </select>
                    </div>
                </div>

                <div
                    class="form-group {{{ $errors->has('content') ? 'has-error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="content">{{
							Lang::get("modal.content") }}</label>
                        <textarea class="form-control full-width wysihtml5" name="content"
                                  value="content" rows="10">{{{ Input::old('content', isset($categories) ? $categories->content : null) }}}</textarea>
                        {{ $errors->first('content', '<label class="control-label">:message</label>')
                        }}
                    </div>
                </div>


                <!-- SOUND -->
                <div class="input-field-group pdf-input sound {{{ $errors->has('sound_reduction') ? 'has-error' : '' }}}">
                    <div class="col-lg-3">
                        <label class="control-label pull-right" for="sound_reduction">Sound reduction</label>
                    </div>
                    <div class="col-lg-6 fileinput fileinput-new">
                        <div class="col-md-12">
                            <input
                                class="form-control" type="text" name="sound_reduction" id="sound_reduction"
                                value="{{{ Input::old('sound_reduction', isset($categories) ? $categories->sound_reduction : null) }}}" />
                            {{$errors->first('sound_reduction', '<label class="control-label">:message</label>')}}
                        </div>
                    </div>
                </div>

                <div class="input-field-group pdf-input sound {{{ $errors->has('sound_absorption') ? 'has-error' : '' }}}">
                    <div class="col-lg-3">
                        <label class="control-label pull-right" for="sound_absorption">Sound absorption</label>
                    </div>
                    <div class="col-lg-6 fileinput fileinput-new">
                        <div class="col-md-12">
                            <input
                                class="form-control" type="text" name="sound_absorption" id="sound_absorption"
                                value="{{{ Input::old('sound_absorption', isset($categories) ? $categories->sound_absorption : null) }}}" />
                            {{$errors->first('sound_absorption', '<label class="control-label">:message</label>')}}
                        </div>
                    </div>
                </div>

                <hr>

                <!-- PDF -->

                <div class="input-field-group pdf-input {{{ $errors->has('pdf') ? 'has-error' : '' }}}">
                    <div class="col-lg-3">
                        <label class="control-label pull-right" for="pdf">Pdf </label>
                    </div>
                    <div class="col-lg-6 fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group">
                            <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                            <span style="background: white" class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select pdf</span><span class="fileinput-exists">Change</span><input id="pdf" type="file" name="pdf"></span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>

                    @if ( isset($categories->id) && file_exists(public_path() . '/appfiles/categories/' . $categories->id . '/pdf_' . $categories->id . '.pdf'))

                    <div class="pdf-info col-lg-3">

                        Current pdf: <a href="{{asset('/appfiles/categories/' . $categories->id . '/pdf_'.$categories->id.'.pdf')}}">download</a> | <a class="remove-category-pdf" href="#" data-type="pdf" data-id="{{ $categories->id }}">remove</a> 
                    </div>
                    @endif
                    {{$errors->first('pdf', '<label class="control-label">:message</label>')}}
                </div>

                
               



                <hr style="margin-bottom:20px;">

                <!-- ./ general tab -->
            </div>
            <!-- ./ tabs content -->

            <!-- Form Actions -->

            <div class="form-group">
                <div class="col-md-12">

                    <button type="submit" class="btn btn-sm btn-success">
                        <span class="glyphicon glyphicon-ok-circle"></span> 
                        @if(isset($categories)) 
                        {{ Lang::get("modal.edit") }} 
                        @else 
                        {{Lang::get("modal.create") }} 
                        @endif
                    </button>
                    @if(Auth::user()->hasRole('super_admin') && isset($categories))

                    <div class="pull-right">
                        <div class="pull-right">
                            <a class="btn btn-sm btn-danger deleteAll" href="{{'/categories/'. $categories->id . '/delete'}}">
                                <span class="glyphicon glyphicon-remove"></span>
                                {{ Lang::get('modal.delete') }}
                            </a>
                        </div>
                    </div>

                    @elseif(Auth::user()->hasRole('admin') && empty($masterParent) && isset($categories))

                    <div class="pull-right">
                        <div class="pull-right">
                            <a class="btn btn-sm btn-danger deleteAll" href="{{'/categories/'. $categories->id . '/delete'}}">
                                <span class="glyphicon glyphicon-remove"></span>
                                {{ Lang::get('modal.delete') }}
                            </a>
                        </div>
                    </div>

                    @endif
                </div>
            </div>
            <!-- ./ form actions -->

            </form>

            @stop