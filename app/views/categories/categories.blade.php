@extends('layouts/main')
<!--Web site Title--> 
@section('title') {{$title}} :: @parent @stop
@section('content')

{{$breadcrumbs}}
@include('notifications')

<div class="pull-right">
    <div class="pull-right">
        <a class="btn btn-sm btn-success" href="/categories/create">
            <span class="glyphicon glyphicon-plus-sign"></span>
            Create new category
        </a>
    </div>
</div>

<h1>{{$title}}</h1>
<hr>

@foreach ($categories as $category)
@if ($category->parent_id == null && $category->id !== '5' )
@if (count($category->subCategories) > 0)
<div class="panel panel-default">
    <div class="panel-body">
        @if($category->id == '1' || $category->id == '2')
        <h4>{{$category->title}} - <small>products</small></h4>
        <div class="pull-right">
            <button type="button" class="btn btn-default btn-sm btnImage" data-toggle="modal" data-target="#imageModal{{$category->id}}">
               @if(isset($category))
                <?php $imageExists = file_exists(public_path() . '/appfiles/categories/' . $category->id . '/main_picture_thumbnail.jpg'); ?>
                @if(!empty($imageExists))
                <span class="glyphicon glyphicon-picture"></span> Edit Image
                @else
                <span class="glyphicon glyphicon-picture"></span> Add Image
                @endif
                @endif
            </button>    
        </div>
        @else
        <h4>{{$category->title}}</h4>
        <div class="pull-right">
            <button type="button" class="btn btn-default btn-sm btnImage" data-toggle="modal" data-target="#imageModal{{$category->id}}">
                @if(isset($category))
                <?php $imageExists = file_exists(public_path() . '/appfiles/categories/' . $category->id . '/main_picture_thumbnail.jpg'); ?>
                @if(!empty($imageExists))
                <span class="glyphicon glyphicon-picture"></span> Edit Image
                @else
                <span class="glyphicon glyphicon-picture"></span> Add Image
                @endif
                @endif
            </button>            
        </div>
        @endif
        <hr>
        <ol>
            @foreach ($category->subCategories as $subCategory)
            @if(Auth::user()->hasRole('super_admin'))
            <li><a href="categories/{{$subCategory->id}}/edit">{{$subCategory->title}}</a></li>
            @else
            <li>{{$subCategory->title}}</li>
            @endif

            @if (count($subCategory->subCategories) > 0)
            <ul type="a" >
                @foreach ($subCategory->subCategories as $subChild)
                <li><a href="categories/{{$subChild->id}}/edit">{{$subChild->title}}</a></li>
                @endforeach
            </ul>
            @endif
            @endforeach
        </ol>
    </div>
</div>
@else
<div class="panel panel-default">
    <div class="panel-body">
        @if($category->id == '1' || $category->id == '2' )
        <h4>{{$category->title}} - <small>products</small></h4>
        @else
        <h4>{{$category->title}}</h4>
        @endif
        <hr>
        <small><i>No categories created</i></small>
    </div>
</div>

@endif
@endif
@endforeach

@for($i=1; $i<5; $i++)
<!-- Modal -->
<div class="modal fade" id="imageModal{{$i}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Image</h4>
            </div>
            <div class="modal-body">

                <!-- MAIN PICTURE -->
                <form class="form-horizontal" enctype="multipart/form-data"
                      method="post"
                      action="{{ URL::to('/categories/image') }}"
                      autocomplete="off">
                    <!-- CSRF Token -->
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                    <!-- ./ csrf token -->
                    <input type="hidden" name="id" value="{{$i}}" />
                    <div style="margin-right: 50px" class="fileinput fileinput-new {{{ $errors->has('image') ? 'has-error' : '' }}}" data-provides="fileinput">
                        <p class="error">
                            @if(isset($category))
                            <?php $imageExists = file_exists(public_path() . '/appfiles/categories/' . $i . '/main_picture_thumbnail.jpg'); ?>
                            @endif
                            {{$errors->first('image', '<label class="control-label">:message</label>')}}
                        </p>
                        <div><label  for="image">Main picture* (min 409x1408)</label></div>                

                        <div class="fileinput-new thumbnail thumbnailCat" >

                            @if(!empty($imageExists))
                            <img id="img" src="{{'/appfiles/categories/' . $i . '/main_picture_thumbnail.jpg'}}" alt="" />
                            @endif
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail thumbnailCat"></div>
                        <div>
                            <span class="btn btn-default btn-file"><span class="fileinput-new">{{ !empty($imageExists) ? 'Change image' : 'Select image' }}</span><span class="fileinput-exists">Change</span><input type="file" name="image"></span>
                            @if(!empty($imageExists))
                            <a class="btn btn-default btn-danger remove-category-pdf" href="#" data-type="img" data-id="{{ $i }}">remove</a>
                            @endif
                        </div>



                    </div>



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save Image</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endfor

@stop