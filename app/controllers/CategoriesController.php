<?php

class CategoriesController extends BaseController {

    public $categoriesService;
    public $productsService;
    public $documentsService;
    public $projectsService;
    public $articlesService;

    public function __construct(ProductsService $productsService, ProjectsService $projectsService, ArticlesService $articlesService, DocumentsService $documentsService, CategoriesService $categoriesService) {
        $this->categoriesService = $categoriesService;
        $this->productsService = $productsService;
        $this->documentsService = $documentsService;
        $this->projectsService = $projectsService;
        $this->articlesService = $articlesService;
    }

    public function index() {
        $categories = $this->categoriesService->getAllCategoriesWithSubcategories();
        $breadcrumbsData = array('pages' => array('categories' => 'Categories'));
        $title = "Categories";
        return View::make('categories.categories', array('categories' => $categories, 'title' => $title))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    public function getAllCategories() {
        return Category::all();
    }

    public function getCreate() {
        $parents = Category::all()->take(2);
        $roots = Category::where('parent_id', NULL)->get();
        $breadcrumbsData = array('pages' => array('categories' => 'Categories',
                'categories/create/' => 'Add new'));
        return View::make('categories.create_edit', compact('roots', 'parents'))->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    public function postCreate() {


        Validator::extend('requireOnePhoto', 'PhotoValidator@requireOnePhoto');
        Validator::extend('resolution', 'PhotoValidator@resolution');
        Validator::extend('pdf', 'PdfValidator@pdf');
        Validator::extend('type', 'PhotoValidator@type');
        $messages = array(
            'require_one_photo' => 'Please upload at least one photo with right resolution.',
            'resolution' => 'Resolution is not right.',
            'pdf' => 'Please use a PDF file.',
            'type' => 'Please upload only PNG or JPG format pictures.'
        );


        $rules = Category::$rules;
        $data = Input::all();
        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {

            return Redirect::back()->withErrors($validator)->withInput();
        }
        $sound_reduction = Input::get('sound_reduction');
        $sound_absorption = Input::get('sound_absorption');

        $parent = Input::get('parent');
        $category = new Category();
        $category->title = Input::get('title');
        $category->content = Input::get('content');
        if (!empty($sound_reduction) && !empty($sound_absorption)) {
            $category->sound_reduction = $sound_reduction;
            $category->sound_absorption = $sound_absorption;
        }
        if (!empty($parent)) {
            $category->parent_id = $parent;
        }
        $category->save();

        $image = Input::file('image');
        $pdf = Input::file('pdf');
        $files = array();
        array_push($files, $pdf, $image);

        foreach ($files as $f) {
            if (!empty($f)) {
                $file = new Document();
                $entityName = get_class();
                $file->store($f, $entityName, $category->id);
            }
        }

        if (Input::hasFile('image')) {
            $ext = $image->getClientOriginalExtension();
            $mainPicture = Image::make('appfiles/categories/' . $category->id . '/photo_' . $category->id . '.' . $ext);
            if ($parent == '1' || $parent == '2' || $parent == '3' || $parent == '4') {
                $mainPicture->fit(409, 1408);
            } else {
                $mainPicture->fit(458, 621);
            }
            $mainPicture->save(public_path() . '/appfiles/categories/' . $category->id . '/main_picture_thumbnail.jpg');
        }
        $this->setMessage(Lang::get('modal.categories.create'));
        return Redirect::to('categories');
    }

    public function getEdit($id) {
        $parents = Category::all()->take(2);
        $categories = Category::find($id);
        $parentId = $categories->masterCategory;

        if (is_null($parentId->parent_id)) {

            $masterParent = $parentId->title;
        } else {
            $masterParent = '';
        }


        $file = Document::where(function($query) use ($id) {
                    $query->where('entity_name', get_class())
                            ->where('entity_id', $id);
                })->first();

        $breadcrumbsData = array('pages' => array('categories' => 'Categories',
                'categories/' . $categories->id . '/edit' => $categories->title . ' edit'));

        return View::make('categories.create_edit', compact('categories', 'parentId', 'file', 'parents', 'masterParent'))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    public function postEdit($id) {

//        Validator::extend('requireOnePhoto', 'PhotoValidator@requireOnePhoto');
        Validator::extend('resolution', 'PhotoValidator@resolution');
        Validator::extend('pdf', 'PdfValidator@pdf');
        Validator::extend('type', 'PhotoValidator@type');
        $messages = array(
//            'require_one_photo' => 'Please upload at least one photo with right resolution.',
            'resolution' => 'Resolution is not right.',
            'pdf' => 'Please use a PDF file.',
            'type' => 'Please upload only PNG or JPG format pictures.'
        );
        $rules = Category::$rules;

        $doc = Document::where(function($query) use ($id) {
                    $query->where('entity_name', get_class())
                            ->where('entity_id', $id)
                            ->where('ext', 'jpg')
                            ->orWhere('ext', 'png')
                            ->orWhere('ext', 'jpeg');
                })->first();

        $data = Input::all();
        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $dirPath = public_path() . '/appfiles/categories/' . $id;
        if (count($doc) > 0) {
            $picturePath = $dirPath . '/photo_' . $id . '.' . $doc->ext;
        }
        $mainPicture = $dirPath . '/main_picture_thumbnail.jpg';
        $pdfPath = $dirPath . '/pdf_' . $id . '.pdf';


        $category = Category::find($id);

        $sound_reduction = Input::get('sound_reduction');
        $sound_absorption = Input::get('sound_absorption');
        $parent = Input::get('parent');
        $category->title = Input::get('title');
        $category->content = Input::get('content');
        $category->parent_id = Input::get('parent');
        if (!empty($sound_reduction) && !empty($sound_absorption)) {
            $category->sound_reduction = $sound_reduction;
            $category->sound_absorption = $sound_absorption;
        }
        $category->save();

        $image = Input::file('image');
        $pdf = Input::file('pdf');


        if (!empty($image)) {
            if (isset($picturePath) && file_exists($picturePath)) {
                unlink($picturePath);
            }
            if (file_exists($mainPicture)) {
                unlink($mainPicture);
            }
        }
        if (!empty($pdf)) {
            if (file_exists($pdfPath)) {
                unlink($pdfPath);
            }
        }

        $files = array();
        array_push($files, $pdf, $image);

        foreach ($files as $f) {
            if (!empty($f)) {
                $file = new Document();
                $entityName = get_class();
                $file->store($f, $entityName, $category->id);
            }
        }

        if (Input::hasFile('image')) {
            $ext = $image->getClientOriginalExtension();
            $mainPicture = Image::make('appfiles/categories/' . $category->id . '/photo_' . $category->id . '.' . $ext);
            if ($parent == '1' || $parent == '2' || $parent == '3' || $parent == '4') {
                $mainPicture->fit(409, 1408);
            } else {
                $mainPicture->fit(458, 621);
            }
            $mainPicture->save(public_path() . '/appfiles/categories/' . $category->id . '/main_picture_thumbnail.jpg');
        }
        $this->setMessage(Lang::get('modal.categories.update'));
        return Redirect::to('categories');
    }

    public function destroy($id) {

        // if subcategories have products        
        $children = Category::where('parent_id', $id)->get();


        if (count($children) > 0) {

            return Redirect::to('categories')->with('error', 'You can not delete categories that have subcategories!');

//            foreach ($children as $child) {
//                $products = Product::where('category_id', $child->id)->get();
//                foreach ($products as $product) {
//                    MachineLanguages::where('machine_id', $product->id)->delete();
//                    $deleteFiles = $this->productsService->deleteFiles($product->id);
//                    $product->delete();
//                }
//                $this->documentsService->deleteFiles($child->id);
//                Document::where('entity_id', $child->id)->delete();
//                $child->delete();
//            }
        }


        $file = Document::where(function($query) use ($id) {
                    $query->where('entity_id', $id)
                            ->where('entity_name', get_class());
                })->delete();


        // if category has products
        $products = Product::where('category_id', $id)->get();
        if (!empty($products)) {
            foreach ($products as $product) {
                MachineLanguages::where('machine_id', $product->id)->delete();
                $deleteFiles = $this->productsService->deleteFiles($product->id);
                $product->delete();
            }
        }

        // if category has projects
        $projects = Project::where('category_id', $id)->get();
        if (!empty($projects)) {
            foreach ($projects as $project) {
                $deleteFiles = $this->projectsService->deleteFiles($project->id);
                $project->delete();
            }
        }

        // if category has articles
        $articles = Article::where('category_id', $id)->get();
        if (!empty($articles)) {
            foreach ($articles as $article) {
                $deleteFiles = $this->articlesService->deleteFiles($article->id);
                $article->delete();
            }
        }


        // delete files
        $this->documentsService->deleteFiles($id);

        Category::destroy($id);

        $this->setMessage(Lang::get('modal.categories.delete'));
        return Redirect::to('categories');
        ;
    }

    public function deletePdf($id, $type) {
        $dirPath = public_path() . '/appfiles/categories/' . $id;
        if ($type == 'pdf') {
            Document::where(function($query) use ($id) {
                $query->where('entity_id', $id)
                        ->where('ext', 'pdf');
            })->delete();
            $filePath = $dirPath  . '/pdf_' . $id . '.pdf';
        } else {
            $docExt = Document::where(function($query) use ($id) {
                        $query->where('entity_id', $id)
                                ->where('ext', 'jpg')
                                ->orWhere('ext', 'png')
                                ->orWhere('ext', 'jpeg');
                    })->first();
            if (count($docExt) > 0) {
                $ext = $docExt->ext;
                $filePath = $dirPath . '/photo_' . $id . '.' . $ext;
                $docExt->delete();
            }
            $mainPicture = $dirPath . '/main_picture_thumbnail.jpg';
        }
        if (isset($filePath) && file_exists($filePath)) {
            unlink($filePath);
        }

        if (isset($mainPicture) && file_exists($mainPicture)) {
            unlink($mainPicture);
        }
         // delete directory
        if (count(glob($dirPath)) === 0 && is_dir($dirPath)) {
            rmdir($dirPath);
        }

        return json_encode(array('deleted' => true));
    }

    public function getAbout() {
        $category_id = Category::find(5)->id;

        $englishLanguageId = Language::where('slug', '=', 'english')->first()->id;

        $machines = Product::with(array('machineLanguages' => function($query) use ($englishLanguageId) {
                        $query->where('machine_languages.language_id', '=', $englishLanguageId);
                    }))
                ->where('category_id', '=', $category_id)
                ->orderBy('place', 'asc')
                ->get();
        $category = Category::find($category_id);

        $breadcrumbsData = array('pages' => array(
                'about-us' => 'About us'));

        return View::make('about.index', array('machines' => $machines, 'category_id' => $category_id, 'category' => $category))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    public function postAbout() {

        Validator::extend('resolution', 'PhotoValidator@resolution');
        $messages = array(
            'resolution' => 'Resolution is not right.',
        );
        $rules = array(
            'image' => 'image|resolution:621, 458|required',
        );
        $data = Input::all();
        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if (Input::hasFile('image')) {
            $id = Input::get('id');
            $image = Input::file('image');
            $imgPth = public_path() . '/appfiles/categories/' . $id . '/';
            $imgName = 'photo_' . $id . '.' . $image->getClientOriginalExtension();
            $image->move($imgPth, $imgName);

            $ext = $image->getClientOriginalExtension();
            $mainPicture = Image::make('appfiles/categories/' . $id . '/photo_' . $id . '.' . $ext);
            $mainPicture->fit(409, 1408);
            $mainPicture->save(public_path() . '/appfiles/categories/' . $id . '/main_picture_thumbnail.jpg');

            $filePath = $imgPth . $imgName;
            if (file_exists($filePath)) {
                unlink($filePath);
            }
        }
        $this->setMessage('Main image successfully created.');
        return Redirect::to('about-us');
    }

    public function postImage() {
        $id = Input::get('id');
        $image = Input::file('image');
        $data = Input::all();
        try {
            $this->categoriesService->storeMainImage($id, $image, $data);
            $this->setMessage('Main image successfully created.');
            return Redirect::to('categories');
            
        } catch (ValidatorImageException $e) {
            $message = $e->getErrors();
            
            $this->setErrorMessage('There are some errors. ' . $message[0]);
            return Redirect::back();
        }
    }

}
