<?php

class ImagesController extends BaseController {

    public function createThumbnail()
    {
        $inputs = Input::all();

        $machineId = $inputs['machineId'];
        $imageName = $inputs['imageName'];
        if($inputs['type'] == 'news'){
           
            $imagePath = public_path().'/appfiles/news/'.$machineId.'/'.$imageName;
        }
        else  if($inputs['type'] == 'projects'){
           
            $imagePath = public_path().'/appfiles/projects/'.$machineId.'/'.$imageName;
        }
        else {
            $imagePath = public_path().'/machines/'.$machineId.'/'.$imageName;
        }
        

        $img = false;
        if (file_exists($imagePath)) {
            $img = Image::make($imagePath);
        }
        
        if ($img) {
            $img->crop(
                intval($inputs['w']),
                intval($inputs['h']),
                intval($inputs['x']),
                intval($inputs['y'])
            );
            
            $pos = strpos($imageName, 'photo');
            if ($pos !== false) {
                $img->fit(627, 940);
            } else {
                $img->fit(460, 620);
            }
                        
            $thumbnailName = str_replace('.', '_thumbnail.', $imageName);
            if($inputs['type'] == 'news'){
                $img->save(public_path().'/appfiles/news/'.$machineId.'/'.$thumbnailName);
            } else if($inputs['type'] == 'projects'){
                 $img->save(public_path().'/appfiles/projects/'.$machineId.'/'.$thumbnailName);
            } else {
                $img->save(public_path().'/machines/'.$machineId.'/'.$thumbnailName);   
            }
            
        } else {
            $this->setErrorMessage('There was error');
        }
    }
    
    

    public function removeImage()
    {
        $inputs = Input::all();

        $machineId = $inputs['machineId'];
        $imageName = $inputs['imageName'];

        $deleted = false;
        if ($this->isThumbnail($imageName)) {
            $imagePath = public_path().'/machines/'.$machineId.'/'.$imageName;
            $deleted = $this->deleteImageFile($imagePath);
        } else {
            if (!$this->isLastImage($machineId)) {
                $imagePath = public_path().'/machines/'.$machineId.'/'.$imageName;
                $deleted = $this->deleteImageFile($imagePath);
                $this->removeThumbnail($machineId, $imageName);
            } else {
                $this->setErrorMessage('You can\'t delete all photos.');
                return json_encode(array('message' => 'error'));
            }
        }

        if ($deleted) {
            $this->setErrorMessage('Successfully deleted.');
            return json_encode(array('message' => 'ok'));
        } else {
            $this->setErrorMessage('There was error.');
            return json_encode(array('message' => 'error'));
        }
    }
    
    
    public function removeImageNews()
    {
        $inputs = Input::all();

        $machineId = $inputs['machineId'];
        $imageName = $inputs['imageName'];
       
        $deleted = false;
        if ($this->isThumbnail($imageName)) {             
            $imagePath = public_path().'/appfiles/news/'.$machineId.'/'.$imageName;
            $deleted = $this->deleteImageFile($imagePath);
        } else {
            if (!$this->isLastImageNews($machineId)) {
                $imagePath = public_path().'/appfiles/news/'.$machineId.'/'.$imageName;
                $deleted = $this->deleteImageFile($imagePath);
                $this->removeThumbnailNews($machineId, $imageName);
            } else {
                $this->setErrorMessage('You can\'t delete all photos.');
                return json_encode(array('message' => 'error'));
            }
        }

        if ($deleted) {
            $this->setErrorMessage('Successfully deleted.');
            return json_encode(array('message' => 'ok'));
        } else {
            $this->setErrorMessage('There was error.');
            return json_encode(array('message' => 'error'));
        }
    }
    
    public function removeImageProject()
    {
        $inputs = Input::all();

        $machineId = $inputs['machineId'];
        $imageName = $inputs['imageName'];
       
        $deleted = false;
        if ($this->isThumbnail($imageName)) {             
            $imagePath = public_path().'/appfiles/projects/'.$machineId.'/'.$imageName;
            $deleted = $this->deleteImageFile($imagePath);
        } else {
            if (!$this->isLastImageProjects($machineId)) {
                $imagePath = public_path().'/appfiles/projects/'.$machineId.'/'.$imageName;
                $deleted = $this->deleteImageFile($imagePath);
                $this->removeThumbnailProject($machineId, $imageName);
            } else {
                $this->setErrorMessage('You can\'t delete all photos.');
                return json_encode(array('message' => 'error'));
            }
        }

        if ($deleted) {
            $this->setErrorMessage('Successfully deleted.');
            return json_encode(array('message' => 'ok'));
        } else {
            $this->setErrorMessage('There was error.');
            return json_encode(array('message' => 'error'));
        }
    }
    
    
    

    private function isThumbnail($imageName)
    {
        return strpos($imageName,'thumbnail') !== false;
    }
    

    private function isLastImage($machineId)
    {
        $numberOfImages = 0;
        for ($i=1; $i < 11; $i++) {
            $imagePath = public_path().'/machines/'.$machineId.'/'.'photo_'.$i.'.jpg';
            if (file_exists($imagePath)) {
                $numberOfImages++;
            }
        }
        return $numberOfImages < 2;
    }
    
    private function isLastImageNews($machineId)
    {
        $numberOfImages = 0;
        for ($i=1; $i < 6; $i++) {
            $imagePath = public_path().'/appfiles/news/'.$machineId.'/'. $machineId .'_photo_'.$i.'.jpg';
            if (file_exists($imagePath)) {
                $numberOfImages++;
            }
        }
        return $numberOfImages < 2;
    }
    
    private function isLastImageProjects($machineId)
    {
        $numberOfImages = 0;
        for ($i=1; $i < 6; $i++) {
            $imagePath = public_path().'/appfiles/news/'.$machineId.'/'. $machineId .'_photo_'.$i.'.jpg';
            if (file_exists($imagePath)) {
                $numberOfImages++;
            }
        }
        return $numberOfImages < 2;
    }

    private function removeThumbnail($machineId, $imageName)
    {
        $thumbnailName = str_replace('.', '_thumbnail.', $imageName);
        $thumbnailPath = public_path().'/machines/'.$machineId.'/'.$thumbnailName;
        $this->deleteImageFile($thumbnailPath);
    }
    
    private function removeThumbnailNews($machineId, $imageName)
    {
        $thumbnailName = str_replace('.', '_thumbnail.', $imageName);
        $thumbnailPath = public_path().'/appfiles/news/'.$machineId.'/'.$thumbnailName;
        $this->deleteImageFile($thumbnailPath);
    }
    
    private function removeThumbnailProject($machineId, $imageName)
    {
        $thumbnailName = str_replace('.', '_thumbnail.', $imageName);
        $thumbnailPath = public_path().'/appfiles/projects/'.$machineId.'/'.$thumbnailName;
        $this->deleteImageFile($thumbnailPath);
    }

    private function deleteImageFile($imagePath)
    {
        if (file_exists($imagePath)) {
            unlink($imagePath);
            return true;
        }
        return false;
    }
}