<?php
use Illuminate\Http\Response as LaravelResponse;

class DeviceTokensController extends \BaseController {

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $token = Input::get('token');

        if (empty($token)) {
            return Response::json(array('message' => 'You need to provide token parameter.'), LaravelResponse::HTTP_BAD_REQUEST);
        }

        $deviceToken = DeviceToken::where('token', '=', $token)->first();

        if (!$deviceToken && $token) {
            DeviceToken::create(array('token' => $token));
        }

        return Response::json(array('message' => 'Token added successfully.'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($token)
	{
        $deviceToken = DeviceToken::where('token', '=', $token)->first();

        if ($deviceToken) {
            $deviceToken->delete();
            return Response::json(array('message' => 'Token deleted successfully.'));
        } else {
            return Response::json(array('message' => 'Token does not exist.'), LaravelResponse::HTTP_NOT_FOUND);
        }
	}
}
