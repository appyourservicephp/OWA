<?php

class ProjectsController extends BaseController {

    public $projectsService;
    public $pushRequestService;
    public function __construct(ProjectsService $projectsService, PushRequestService $pushRequestService) {
        $this->projectsService = $projectsService;
         $this->pushRequestService = $pushRequestService;
    }

    /*
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index() {
        $cat = Category::where('title', 'Projects')->first();

        $parent = $this->loop($cat->id);

        $breadcrumbsData = array('pages' => array('projects' => 'Projects'));
        $title = "Projects";
        return View::make('projects.index', array('title' => $title, 'parents' => $parent))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    public function loop($id) {
        $arr = array();
        $results = Category::where('parent_id', $id)->get();
        foreach ($results as $result) {
            $arr[] = array(
                'title' => $result->title,
                'id' => $result->id,
                'children' => $this->loop($result->id)
            );
        }
        return $arr;
    }

    public function show($id) {
        $parent = Category::find($id);

        $projects = Project::where('category_id', $parent->id)->get();

        $children = Category::where('parent_id', $id)->get();

        $breadcrumbsData = array('pages' => array('projects' => 'Projects', 'projects/' . $parent->id . '/show' => $parent->title));

        return View::make('projects.show', array('children' => $children, 'parent' => $parent, 'projects' => $projects))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate($id) {
        $parent = Category::find($id);
        $breadcrumbsData = array('pages' => array('projects' => 'Projects', 'projects/' . $parent->id . '/show' => $parent->title, 'projects/' . $parent->id . '/new' => 'Add new'));
        return View::make('projects.create_edit', compact('parent'))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate() {
        $parent_id = Input::get('id');
        try {
            $projects = $this->projectsService->store(Input::all(), get_class());

            $this->setMessage(Lang::get('projects.projects.create'));
            $this->pushRequestService->push($projects->id);            
            return Redirect::to('projects/' . $parent_id . '/show');
        } catch (ValidatorException $e) {
            $this->setErrorMessage('There are some errors.');

            return Redirect::back()
                            ->withErrors($e->getErrors())
                            ->withInput(Input::except('pictures', 'header_picture', 'pdf', 'video'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getEdit($id) {
        $project = Project::find($id);
        $parent = Category::where('id', $project->category_id)->first();
//        $parent = $category->masterCategory;
        $breadcrumbsData = array('pages' => array('projects' => 'Projects', 'projects/' . $parent->id . '/show' => $parent->title,
                'projects/' . $project->id . '/edit' => 'Edit ' . $project->title));

        return View::make('projects.create_edit', compact('parent', 'project'))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
        ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postEdit($id) {
        
        $parent_id = Input::get('id');        
                
        try {
           
            $project = $this->projectsService->update(Input::all(), $id );            
            $this->setMessage(Lang::get('projects.projects.edit'));
            $this->pushRequestService->push($id);            
            return Redirect::to('projects/' . $parent_id . '/show');
        } catch (ValidatorException $e) {
            $this->setErrorMessage('There are some errors.');
            
            return Redirect::back()
                            ->withErrors($e->getErrors())
                            ->withInput(Input::except('pictures', 'header_picture', 'pdf', 'video'));
        }

    }

    public function delete($id) {

        $project = Project::find($id);
        Product::where('category_id', $project->category_id)->delete();
        $this->projectsService->deleteFiles($project->id);
        $project->delete();
        $this->setMessage(Lang::get('projects.projects.delete'));
        return Redirect::back();
    }
    
    public function deleteFile($id, $i, $type)
    {
        
        $isFileDeleted = $this->projectsService->deleteFile($id, $i, $type);

        return json_encode(array('deleted' => $isFileDeleted));
    }
    
     public function getAllProjects() {
       
            return  Project::all();
       
    }


}
