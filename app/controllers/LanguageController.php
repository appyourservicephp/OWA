<?php

class LanguageController extends BaseController {

    public function getAllLanguages()
    {
        return Language::where('required', '=', true)->get();
    }

    public function getLanguages($languageId)
    {
        return Language::where('id', $languageId)
                       ->orWhere('slug', $languageId)
                       ->with(array(
                            'machineLanguages' => function($query) {
                                $query->join('machines', 'machine_id', '=', 'machines.id')
                                ->orderBy('category_id', 'asc')
                                ->orderBy('place', 'asc');
                            },
                            'machineLanguages.machine.category'))
                       ->get();
    }
}
