<?php

class ProductsController extends BaseController {

    public $productsService;
    public $pushRequestService;
    public $historyService;

    public function __construct(ProductsService $productsService, PushRequestService $pushRequestService, HistoryService $historyService) {
        $this->productsService = $productsService;
        $this->pushRequestService = $pushRequestService;
        $this->historyService = $historyService;
    }

    public function index($id) {
        
        
        $category_id = Category::find($id)->id;

        $englishLanguageId = Language::where('slug', '=', 'english')->first()->id;

        $machines = Product::with(array('machineLanguages' => function($query) use ($englishLanguageId) {
                        $query->where('machine_languages.language_id', '=', $englishLanguageId);
                    }))
                ->where('category_id', '=', $category_id)
                ->orderBy('place', 'asc')
                ->get();
        $category = Category::find($category_id);

        $breadcrumbsData = array('pages' => array('products' => 'Products',
                'products/' . $category_id . '/show' => Category::find($category_id)->title . ' management'));

        return View::make('products.index', array('machines' => $machines, 'category_id' => $category_id, 'category' => $category))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    public function show() {
        $cat1 = Category::find(1);
        $cat2 = Category::find(2);
        if (!empty($cat1)) {
            $parents1 = $this->loop($cat1->id);           
        } else {
            $parents1 = '';
        }
        
        if (!empty($cat2)) {
            $parents2 = $this->loop($cat2->id);           
        } else {
            $parents2 = '';
        }

        $breadcrumbsData = array('pages' => array('products' => 'Products'));
        $title = "Products";
        return View::make('products.show', array('title' => $title, 'parents1' => $parents1, 'parents2' => $parents2))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    public function loop($id) {
        $arr = array();
        $results = Category::where('parent_id', $id)->get();
        foreach ($results as $result) {
            $arr[] = array(
                'title' => $result->title,
                'id' => $result->id,
                'parent' => $result->masterCategory->title,
                'children' => $this->loop($result->id)
            );
        }
        return $arr;
    }

    public function showAdd($category_id) {
        $languages = Language::orderBy('required', 'desc')
                ->orderBy('slug', 'asc')
                ->get();
        $requiredLanguageIds = Language::where('required', '=', true)->lists('id');


        $category = Category::find($category_id);

        $categoryTitle = Category::find($category_id)->title;

        $breadcrumbsData = array('pages' => array('products' => 'Products',
                'products/' . $category_id . '/show' => $categoryTitle . '  management',
                'products/' . $category_id . '/new' => $categoryTitle . ' new'));

        return View::make('products.add', array('languages' => $languages, 'category_id' => $category_id, 'category' => $category, 'requiredLanguageIds' => $requiredLanguageIds, 'randNumber' => time()))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    public function doAdd() {

        try {
            $machine = $this->productsService->store(Input::all());

            $this->setMessage('New product created successfully.');
            $this->pushRequestService->push($machine->id);
            $this->historyService->store($machine->id, $this->getProducts($machine->id)->toJson());
            if (Input::input('category_id') == '5') {
                return Redirect::to('/about-us/');
            }
            return Redirect::to('/products/' . Input::input('category_id') . '/show');
        } catch (ValidatorException $e) {
            $this->setErrorMessage('There are some errors.');
//            echo '<pre>';
//            print_r($e->getErrors()); exit;
            return Redirect::back()
                            ->withErrors($e->getErrors())
                            ->withInput(Input::except('main_picture', 'header_picture', 'pictures', 'pdf', 'docx', 'videos'));
        }
    }
    
    public function showAddAbout($category_id) {
        $languages = Language::orderBy('required', 'desc')
                ->orderBy('slug', 'asc')
                ->get();
        $requiredLanguageIds = Language::where('required', '=', true)->lists('id');


        $category = Category::find($category_id);

        $categoryTitle = Category::find($category_id)->title;

        $breadcrumbsData = array('pages' => array('about-us' => 'About us',
                
                'about-us/' . $category_id . '/new' =>  'product new'));

        return View::make('about.add', array('languages' => $languages, 'category_id' => $category_id, 'category' => $category, 'requiredLanguageIds' => $requiredLanguageIds, 'randNumber' => time()))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }
    public function showEditAbout($category_id, $machine_id) {

        $languages = Language::orderBy('required', 'desc')
                ->orderBy('slug', 'asc')
                ->get();
        $requiredLanguageIds = Language::where('required', '=', true)->lists('id');

        // get all languages with machine languages id of $machine_id
        $languages = Language::with(array('machineLanguages' => function($query) use ($machine_id) {
                        $query->where('machine_languages.machine_id', '=', $machine_id);
                    }, 'machineLanguages.machine.category'))->get();

        $data = array();
        foreach ($languages->toArray() as $language) {
            $data[$language['id']] = $language['machine_languages'][0];
        }

        $categoryTitle = Category::find($category_id)->title;
        $productTitle = MachineLanguages::where('machine_id', $machine_id)->first();

        $category = Category::find($category_id);

        $breadcrumbsData = array('pages' => array('about-us' => 'About us',
                'about-us/' . $category_id . '/edit/' . $productTitle->machine_id  =>  $productTitle->name,
               ));

        return View::make('about.edit', array('languages' => $languages,
                            'category_id' => $category_id,
                            'category' => $category,
                            'machine_id' => $machine_id,
                            'productTitle' => $productTitle,
                            'data' => $data,
                            'requiredLanguageIds' => $requiredLanguageIds))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }
    

    public function showEdit($category_id, $machine_id) {

        $languages = Language::orderBy('required', 'desc')
                ->orderBy('slug', 'asc')
                ->get();
        $requiredLanguageIds = Language::where('required', '=', true)->lists('id');

        // get all languages with machine languages id of $machine_id
        $languages = Language::with(array('machineLanguages' => function($query) use ($machine_id) {
                        $query->where('machine_languages.machine_id', '=', $machine_id);
                    }, 'machineLanguages.machine.category'))->get();

        $data = array();
        foreach ($languages->toArray() as $language) {
            $data[$language['id']] = $language['machine_languages'][0];
        }

        $categoryTitle = Category::find($category_id)->title;
        $productTitle = MachineLanguages::where('machine_id', $machine_id)->first();

        $category = Category::find($category_id);

        $breadcrumbsData = array('pages' => array('products' => 'Products',
                'products/' . $category_id . '/show' => $categoryTitle . ' management',
                $productTitle->name));

        return View::make('products.edit', array('languages' => $languages,
                            'category_id' => $category_id,
                            'category' => $category,
                            'machine_id' => $machine_id,
                            'productTitle' => $productTitle,
                            'data' => $data,
                            'requiredLanguageIds' => $requiredLanguageIds))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    public function doEdit() {


        try {
            $oldMachine = $this->getProducts(Input::get('machine_id'))->toJson();
            $machine = $this->productsService->update(Input::all());
            $this->setMessage('Product updated successfully.');
            if ($machine['machineUpdate'] == true) {
                
//                //$this->pushRequestService->push(Input::get('machine_id'));
//                $this->historyService->update(Input::get('machine_id'), $oldMachine);
            } else if (count($machine['languageUpdate']) > 0) {
                $this->pushRequestService->push(Input::get('machine_id'), $machine['languageUpdate']);
//                //$this->historyService->update(Input::get('machine_id'), $oldMachine);
            }
            if (Input::input('category_id') == '5') {
                return Redirect::to('/about-us/');
            }
            return Redirect::to('/products/' . Input::input('category_id') . '/show');
        } catch (ValidatorException $e) {
            $this->setErrorMessage('There are some errors.');
           
            return Redirect::back()
                            ->withErrors($e->getErrors())
                            ->withInput(Input::except('main_picture', 'header_picture', 'pictures', 'videos', 'pdf', 'docx'));
        }
    }

    public function updatePlace() {
        foreach (Input::get('ids') as $index => $id) {
            Product::find($id)->update(array('place' => $index + 1));
        }
    }

    public function delete($machine_id) {
        // delete machine
        $machine = Product::find($machine_id);
        $machine->delete();


        // delete machine history
        $machine = History::where('machine_id', '=', $machine_id)->delete();

        // delete machine languages
        $machineLanguages = MachineLanguages::where('machine_id', '=', $machine_id)->delete();

        // delete files
        $this->productsService->deleteFiles($machine_id);

        $this->setMessage('Product deleted successfully.');
        return Redirect::back();
    }

    public function getProducts($id) {
        $query = MachineLanguages::join('products', 'machine_id', '=', 'products.id')->where('machine_id', $id);

//        $lang = Input::get('lang');
//
//        if (!empty($lang)) {
//            $langs = explode(',', $lang);
//            $query->with(array('MachineLanguages' => function($q) use ($langs) {
//                    $q->join('languages', 'language_id', '=', 'languages.id')
//                            ->whereIN('machine_languages.language_id', $langs)
//                            ->orWhereIN('languages.slug', $langs);
//                }));
//        } else {
//            $query->with('MachineLanguages');
//        }

        return $results = $query->get();
    }

    public function getAllProducts() {
        $query = MachineLanguages::join('products', 'machine_id', '=', 'products.id');
        
        $products = $query->get();
        $articles = Article::select('id', 'title as name', 'content as intro', 'category_id', 'machine_id')->get();
        $projects = Project::select('id', 'title as name', 'content as intro', 'category_id', 'machine_id')->get();
        
        $results = array();
        
        foreach($products as $product) {
            $results[] = array($product);
        };
        foreach($articles as $article) {
            $results[] = array($article);
        };
        foreach($projects as $project) {
            $results[] = array($project);
        };
        
        return $results;
    }

    public function deleteFile($machine, $language, $i, $type) {
        $isFileDeleted = $this->productsService->deleteFile($machine, $language, $i, $type);

        return json_encode(array('deleted' => $isFileDeleted));
    }

//    public function deleteVideo($machine, $language, $video)
//    {
//        $isFileDeleted = $this->productsService->deleteVideo($machine, $language, $video);
//
//        return json_encode(array('deleted' => $isFileDeleted));
//    }

    public function lceformfileds() {

        return View::make('products.lceformfileds', array('randNumber' => time()));
    }

}
