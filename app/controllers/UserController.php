<?php

/*
  |--------------------------------------------------------------------------
  | Confide Controller Template
  |--------------------------------------------------------------------------
  |
  | This is the default Confide controller template for controlling user
  | authentication. Feel free to change to your needs.
  |
 */

class UserController extends BaseController {

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * @var UserRepository
     */
    protected $userRepo;

    public function __construct(User $user, UserRepository $userRepo) {
        parent::__construct();
        $this->user = $user;
        $this->userRepo = $userRepo;
    }

    public function index() {
        return View::make('login');
    }

    public function show() {
        $title = 'Users';
        $breadcrumbsData = array('pages' => array('users' => 'Users',));
        return View::make('users.index', compact('title'))->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    /**
     * Displays the form for account creation
     *
     */
    public function getCreate() {
        $title = 'Create new user';
        $breadcrumbsData = array('pages' => array('users' => 'Users', 'user/create' => 'New user',));
        $roles = Role::all();
        $selectedRoles = array();
        return View::make('users.create_edit', compact('title', 'roles', 'selectedRoles'))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    /**
     * Stores new account
     *
     */
    public function postCreate() {

        $input = Input::all();

        $v = Validator::make($input, User::$rules);

        if ($v->fails()) {
            return Redirect::back()->withErrors($v);
        }
        $user = new User;
        $user->username = Input::get('username');
        $user->email = Input::get('email');
        $user->password = Input::get('password');
        $user->password_confirmation = Input::get('password_confirmation');
        $user->confirmation_code = str_random(32);
        $user->confirmed = Input::get('confirmed');
        $user->save();

        foreach (Input::get('roles') as $item) {
            $role = new AssignedRoles();
            $role->role_id = $item;
            $role->user_id = $user->id;
            $role->save();
        }

        $this->setMessage(Lang::get('users.msg.create'));
        return Redirect::to('/users');
    }

    public function getEdit($id) {
        $title = $user = User::find($id);
        $title = 'Edit ' . $user->username;
        $roles = Role::all();
        $selectedRoles = AssignedRoles::where('user_id', '=', $user->id)->lists('role_id');

        $breadcrumbsData = array('pages' => array('users' => 'Users', 'user/' . $user->id . '/edit' => 'Edit ' . $user->username));

        return View::make('users.create_edit', compact('user', 'title', 'roles', 'selectedRoles'))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    public function postEdit($id) {
       
        $input = Input::all();
        
        $v = Validator::make($input, User::$rules);

        if ($v->fails()) {
            return Redirect::back()->withErrors($v);
        }
                
        $user = User::find($id);
        $user->username = Input::get('username');
        $user->confirmed = Input::get('confirmed');       
       

        $password = Input::get('password');
        $passwordConfirmation = Input::get('password_confirmation');
        $user->password_confirmation = $passwordConfirmation;
        $user->password = $password;
           
        $user->save();
        
       
        
        AssignedRoles::where('user_id', '=', $user->id)->delete();
        foreach (Input::get('roles') as $item) {
            $role = new AssignedRoles;
            $role->role_id = $item;
            $role->user_id = $user->id;
            $role->save();
        }
        
        $this->setMessage(Lang::get('users.msg.edit'));
        return Redirect::to('/users');
        
    }

    /**
     * Displays the login form
     *
     */
    public function login() {
        if (Confide::user()) {
// If user is logged, redirect to internal 
// page, change it to '/admin', '/dashboard' or something
            return Redirect::to('/categories');
        } else {
            return View::make('login');
        }
    }

    /**
     * Attempt to do login
     *
     */
    public function do_login() {
        $repo = App::make('UserRepository');
        $input = Input::all();

        if ($this->userRepo->login($input)) {

            return Redirect::intended('/categories');
        } else {
            
            if ($this->userRepo->isThrottled($input)) {
                $err_msg = Lang::get('confide::confide.alerts.too_many_attempts');
            } elseif ($this->userRepo->existsButNotConfirmed($input)) {
                $err_msg = Lang::get('confide::confide.alerts.not_confirmed');
            } else {
                $err_msg = Lang::get('confide::confide.alerts.wrong_credentials');
           
            }
            
            $this->setErrorMessage($err_msg);
            return Redirect::to('user/login')
                            ->withInput(Input::except('password'))
                            ->with('error', $err_msg);
        }
    }

    /**
     * Attempt to confirm account with code
     *
     * @param  string  $code
     */
    public function confirm($code) {
        if (Confide::confirm($code)) {
            $notice_msg = Lang::get('confide::confide.alerts.confirmation');
            return Redirect::action('UserController@login')
                            ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_confirmation');
            return Redirect::action('UserController@login')
                            ->with('error', $error_msg);
        }
    }

    /**
     * Displays the forgot password form
     *
     */
    public function forgot_password() {
        return View::make(Config::get('confide::forgot_password_form'));
    }

    /**
     * Attempt to send change password link to the given email
     *
     */
    public function do_forgot_password() {
        if (Confide::forgotPassword(Input::get('email'))) {
            $notice_msg = Lang::get('confide::confide.alerts.password_forgot');
            return Redirect::action('UserController@login')
                            ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_forgot');
            return Redirect::action('UserController@forgot_password')
                            ->withInput()
                            ->with('error', $error_msg);
        }
    }

    /**
     * Shows the change password form with the given token
     *
     */
    public function reset_password($token) {
        return View::make(Config::get('confide::reset_password_form'))
                        ->with('token', $token);
    }

    /**
     * Attempt change password of the user
     *
     */
    public function do_reset_password() {
        $input = array(
            'token' => Input::get('token'),
            'password' => Input::get('password'),
            'password_confirmation' => Input::get('password_confirmation'),
        );

// By passing an array with the token, password and confirmation
        if (Confide::resetPassword($input)) {
            $notice_msg = Lang::get('confide::confide.alerts.password_reset');
            return Redirect::action('UserController@login')
                            ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_reset');
            return Redirect::action('UserController@reset_password', array('token' => $input['token']))
                            ->withInput()
                            ->with('error', $error_msg);
        }
    }

    /**
     * Log the user out of the application.
     *
     */
    public function logout() {
        Confide::logout();

        return Redirect::to('/user/login');
    }

    public function delete($id) {
        $user = User::find($id);
        $user->delete();
        $this->setMessage(Lang::get('users.msg.delete'));
        return Redirect::to('users');
    }

    public function data() {
        $users = User::select(array('users.id', 'users.username', 'users.email', 'users.confirmed'))->orderBy('users.email', 'ASC');
        //$users = User::select(array('users.id','users.name','users.email', 'users.created_at'))->orderBy('users.email', 'ASC');

        return Datatables::of($users)
                        ->edit_column('confirmed', '@if ($confirmed=="1") <span class="glyphicon glyphicon-ok"></span> @else <span class=\'glyphicon glyphicon-remove\'></span> @endif')
                        ->add_column('actions', '
                            @if(Auth::user()->hasRole("super_admin"))
                    <a href="{{{ URL::to(\'user/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-sm" ><span class="glyphicon glyphicon-pencil"></span>Edit</a>
                    <a href="{{{ URL::to(\'user/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger delete"><span class="glyphicon glyphicon-trash"></span>Delete</a>
                        @else
                        
                        @endif
                ')
                        ->remove_column('id')
                        ->make(true);
    }

}
