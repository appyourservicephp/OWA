<?php

class LogoController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $title = "Change Logo";


        return View::make('logo.index', compact('title'));
    }

    public function getAllLogos() {
        return Logo::all();
    }

    public function getShortcode() {

        $code = Input::get('code');

        $query = Logo::where('shortcode', $code)->first();

        if(!empty($query)) {
            return $query;
        } else {
           return Response::view('errors.missing', array(), 404);
        }
        
        
    }

    public function create() {
        $rules = array(
            'title' => 'required',
            'shortcode' => 'required|unique:logo',
            'picture' => 'required|image',
        );

        $v = Validator::make(Input::all(), $rules);
        if ($v->fails()) {
            return Redirect::back()->withErrors($v->messages());
        }
        $logo = new Logo();
        $logo->title = Input::get('title');
        $logo->shortcode = Input::get('shortcode');
        $logo->save();
        $entity_name = get_class($logo);

        $file = new Document();
        $file->store(Input::file('picture'), $entity_name, $logo->id);
        $this->setMessage('You have successfully added a logo');
        return Redirect::to('/logo');
    }

    public function destroy($id) {
        $dirPath = public_path() . '/appfiles/logo/' . $id;
        $file = Document::where('entity_id', $id)->first();

        $filePath = $dirPath . '/logo_' . $id . '.' . $file->ext;

        if (file_exists($filePath)) {
            unlink($filePath);
        }
        // delete directory
        if (is_dir($dirPath)) {
            rmdir($dirPath);
        }
        $file->delete();

        Logo::find($id)->delete();

        $this->setMessage('You have successfully deleted a logo');
        return Redirect::to('/logo');
    }

    public function data() {
        $shortcodes = Logo::join('documents', 'documents.entity_id', '=', 'logo.id')->select(array('logo.id', 'logo.title', 'logo.shortcode', 'documents.name'));


        return Datatables::of($shortcodes)
                        ->add_column('actions', '
                            @if(Auth::user()->hasRole("super_admin"))
                   
                    <a href="{{{ URL::to(\'logo/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger delete"><span class="glyphicon glyphicon-trash"></span>Delete</a>
                        @else
                        
                        @endif
                ')
                        ->remove_column('id')
                        ->make(true);
    }

}
