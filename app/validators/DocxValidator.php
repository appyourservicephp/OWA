<?php

class DocxValidator
{
    public function requireOneDocx($field, $value, $params)
    {
         
        foreach ($value as $key => $val) {
            if ($val != null) {
                if($this->pdf($field, $val, $params) ) {
                    return true;
                }
            }
            
            return false;
        }
        
    }
    
    public function docx($field, $value, $params)
    {
       $v = $value->getClientOriginalExtension();
       $type = array('docx');
        if (isset($value) && in_array($v, $type) ) {            
            return true;
        }
        
        return false;
    }
    
    
    
    
    
}
