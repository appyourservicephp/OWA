<?php

class PdfValidator
{
    public function requireOnePdf($field, $value, $params)
    {
         
        foreach ($value as $key => $val) {
            if ($val != null) {
                if($this->pdf($field, $val, $params) ) {
                    return true;
                }
            }
            
            return false;
        }
        
    }
    
    public function pdf($field, $value, $params)
    {
       
        if (isset($value) && $value->getClientOriginalExtension() == 'pdf') {
            
            return true;
        }
        
        return false;
    }
    
    
    
    
    
}
