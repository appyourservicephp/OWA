<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('machine_languages', function(Blueprint $table)
		{           
                         $table->dropColumn('designer');
                         $table->dropColumn('country');
                         
                         $table->string('material');
			 $table->string('reaction_to_fire');
                         $table->string('thickness');
                         $table->string('colour');
                         $table->string('light_reflexion');
                         $table->string('sound_reduction');
                         $table->string('sound_absorption');
                         $table->string('resistance_to_fire');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('machine_languages', function(Blueprint $table)
		{
                         $table->dropColumn('material');
			 $table->dropColumn('reaction_to_fire');
			 $table->dropColumn('thickness');
                         $table->dropColumn('colour');
                         $table->dropColumn('light_reflexion');
                         $table->dropColumn('sound_reduction');
                         $table->dropColumn('sound_absorption');
                         $table->dropColumn('resistance_to_fire');
		});
	}

}
