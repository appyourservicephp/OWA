<?php


class RolesTableSeeder extends Seeder {

    public function run() {
        DB::table('roles')->delete();

        $adminRole = new Role;
        $adminRole->name = 'super_admin';
        $adminRole->display_name = 'super admin';
        $adminRole->description = 'description';
        $adminRole->is_super_admin = 1;
        $adminRole->save();

        $commentRole = new Role;
        $commentRole->name = 'admin';
        $commentRole->display_name = 'admin';
        $commentRole->description = 'description';
        $commentRole->is_super_admin = 0;
        $commentRole->save();

        $user = User::where('email', '=', 'admin@example.org')->first();
        $assignedrole = new AssignedRoles;
        $assignedrole->user_id = $user->id;
        $assignedrole->role_id = $adminRole->id;
        $assignedrole->save();

        $user = User::where('email', '=', 'user@example.org')->first();
        $assignedrole = new AssignedRoles;
        $assignedrole->user_id = $user->id;
        $assignedrole->role_id = $commentRole->id;
        $assignedrole->save();
    }

}
