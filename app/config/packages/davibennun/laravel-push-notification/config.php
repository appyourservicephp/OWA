<?php

return array(

    'owa'=>array(
        'environment' => 'development',
        'certificate' => base_path().'/certs.pem',
        'passPhrase' => 'asdf',
        'service' => 'apns'
    ),

    'owa_prod' => array(
        'environment' => 'production',
        'certificate' => base_path().'/certs.pem',
        'passPhrase' => 'certs',
        'service' => 'apns'
    )
);
